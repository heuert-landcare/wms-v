import 'ol/ol.css';
import {
    Map,
    View
} from 'ol';
import OSM from 'ol/source/OSM';
import {
    Image as ImageLayer,
    Tile as TileLayer
} from 'ol/layer.js';
import Projection from 'ol/proj/Projection.js';
import ImageWMS from 'ol/source/ImageWMS.js';
import {getCenter, getWidth, getHeight, getForViewAndSize, containsExtent} from 'ol/extent.js'
import {DEFAULT_WMS_VERSION} from 'ol/source/common.js'
import {assign} from 'ol/obj.js'
// import ImageBase from 'ol/ImageBase.js'
// import ImageState from 'ol/ImageState.js'
// import {listen} from 'ol/events.js'
// import EventType from 'ol/events/EventType.js';
import ImageWrapper from './Video.js'

const layer = new ImageLayer({
    source: new ImageWMS({
        attributions: '© <a href="http://www.geo.admin.ch/internet/geoportal/' +
            'en/home.html">National parks / geo.admin.ch</a>',
        crossOrigin: 'anonymous',
        params: {
            'LAYERS': 'ch.bafu.schutzgebiete-paerke_nationaler_bedeutung'
        },
        serverType: 'mapserver',
        url: 'https://wms.geo.admin.ch/'
    })
})
class VideoLayer extends ImageLayer {
    constructor(options) {
        super(options)
    }
}
class VideoWMS extends ImageWMS {
    constructor(options) {
        super(options)
    }

  getImageInternal(extent, resolution, pixelRatio, projection) {
      if (this.url_ === undefined) {
          return null;
      }
      resolution = this.findNearestResolution(resolution);

      if (pixelRatio != 1 && (!this.hidpi_ || this.serverType_ === undefined)) {
          pixelRatio = 1;
      }

      var imageResolution = resolution / pixelRatio;

      var center = getCenter(extent);
      var viewWidth = Math.ceil(getWidth(extent) / imageResolution);
      var viewHeight = Math.ceil(getHeight(extent) / imageResolution);
      var viewExtent = getForViewAndSize(center, imageResolution, 0,
          [viewWidth, viewHeight]);
      var requestWidth = Math.ceil(this.ratio_ * getWidth(extent) / imageResolution);
      var requestHeight = Math.ceil(this.ratio_ * getHeight(extent) / imageResolution);
      var requestExtent = getForViewAndSize(center, imageResolution, 0,
          [requestWidth, requestHeight]);

      var image = this.image_;
      if (image &&
          this.renderedRevision_ == this.getRevision() &&
          image.getResolution() == resolution &&
          image.getPixelRatio() == pixelRatio &&
          containsExtent(image.getExtent(), viewExtent)) {
          return image;
      }

      var params = {
          'SERVICE': 'WMS',
          'VERSION': DEFAULT_WMS_VERSION,
          'REQUEST': 'GetMap',
          'FORMAT': 'image/png',
          'TRANSPARENT': true
      };
      assign(params, this.params_);

      this.imageSize_[0] = Math.round(getWidth(requestExtent) / imageResolution);
      this.imageSize_[1] = Math.round(getHeight(requestExtent) / imageResolution);

      var url = this.getRequestUrl_(requestExtent, this.imageSize_, pixelRatio,
          projection, params);

      this.image_ = new ImageWrapper(requestExtent, resolution, pixelRatio,
          url, this.crossOrigin_, this.imageLoadFunction_);

      this.renderedRevision_ = this.getRevision();

    //   listen(this.image_, EventType.CHANGE,
    //       this.handleImageChange, this);

      return this.image_;

  }
}
const source = new VideoWMS({
    attributions: '© <a href="https://www.landcareresearch.co.nz' +
        'en/home.html">Manaaki Whenua - Landcare Research</a>',
    crossOrigin: 'anonymous',
    params: {
        'LAYERS': 'onlyonefornow'
    },
    serverType: 'mapserver',
    url: 'http://wms-v.tk/wms-v-composite',
    ratio: 1
})
const videoLayer = new VideoLayer({
    source
})
var projection = new Projection({
    code: 'EPSG:21781',
    units: 'm'
});
const resolutions = [4, 2, 1] // [...Array(4).keys()].map(r => 1 / Math.pow(2, r))
const map = new Map({
    target: 'map',
    layers: [
        new TileLayer({
            source: new OSM()
        }),
        // layer,
        videoLayer
    ],
    view: new View({
        // center: [103789121.91533035 - 50000, 89012365.57484263],
        center: [0, 0],
        resolutions,
        projection: projection,
        zoom: 1
    })
});
window.map = map
window.view = map.getView()
window.source = source
map.on("postcompose", event => {
    const ctx = event.context
    const frameState = event.frameState
    const pixelRatio = 1 // frameState.pixelRatio
    const video = source.image_.image_
    const extent = frameState.extent
    const topLeft = [extent[0], extent[3]]
    const coord = map.getPixelFromCoordinate(topLeft) // frameState.pixelToCoordinateTransform.slice(4)
    //videoLayer
    ctx.drawImage(video, coord[0] * pixelRatio, coord[1] * pixelRatio, 
        pixelRatio * video.videoWidth, pixelRatio * video.videoHeight)
    if (source.image_.image_.paused) {
        source.image_.image_.muted = true
        source.image_.image_.play()
    }
})

setInterval(() => map.render(), 1000 / 24)