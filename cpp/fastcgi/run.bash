#!/bin/bash

docker run -d -p 8000:80 \
--mount type=bind,source=`pwd`,target=/app \
-v `pwd`/default-nginx.conf:/etc/nginx/nginx.conf \
--name wms-v-c wms-v