#include <stdlib.h>
#include <iostream>
#include <string>
#include "fcgio.h"

using namespace std;

// Maximum bytes
const unsigned long STDIN_MAX = 1000000;

/**
 * Note this is not thread safe due to the static allocation of the
 * content_buffer.
 */
int main()
{
    // cout << "Status: 200\r\nContent-type: image/jpeg\r\nContent-Length: 577182\r\n\r\n";
    // auto file = popen("/bin/cat /app/000001.mp4", "rb");
    FILE *file = fopen("/app/logo.jpg", "rb");
    // char buffer[1024*32];
    char buffer[256];
    while (!feof(file))
    {
        auto r = fread(buffer, sizeof(buffer), 1, file);
        // cout << buffer;
        cout.write(buffer, sizeof(buffer));
        if (!r) break;
    }
    // pout = popen("/bin/ls -la /app", "r");
    fclose(file);

    // cout << "  </body>\n"
    //      << "</html>\n";
    // Note: the fcgi_streambuf destructor will auto flush
    return 0;
}
