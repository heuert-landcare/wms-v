import olMap from 'ol/Map.js';
import olView from 'ol/View.js';
import ImageWMS from 'ol/source/ImageWMS.js'
import ImageLayer from 'ol/layer/Image.js'
import proj4 from 'proj4'
import {register} from 'ol/proj/proj4.js'
import {Projection} from 'ol/proj.js'
import WMTSTileGrid from 'ol/tilegrid/WMTS.js'
import TileGrid from 'ol/tilegrid/TileGrid.js'
import TileLayer from 'ol/layer/Tile.js'
import WMTS from 'ol/source/WMTS.js'
import {defaults as defaultControls} from 'ol/control.js';
import TileWMS from 'ol/source/TileWMS.js'
import ImageTile from 'ol/ImageTile.js'
import Tile from 'ol/Tile.js'
import TileState from 'ol/TileState.js'
import {listenOnce, unlistenByKey, listen} from 'ol/events.js';
import EventType from 'ol/events/EventType.js';
import * as coordinate from 'ol/coordinate.js'
import {createCanvasContext2D} from 'ol/dom.js'
import {TileDebug} from 'ol/source.js'

const VideoTile = (function (Tile) {
  function getBlankImage() {
    var ctx = createCanvasContext2D(1, 1);
    ctx.fillStyle = 'rgba(0,0,0,0)';
    ctx.fillRect(0, 0, 1, 1);
    return ctx.canvas.toDataURL('image/png');
  }

  function VideoTile(tileCoord, state, src, crossOrigin, tileLoadFunction, opt_options) {

    Tile.call(this, tileCoord, state, opt_options);

    /**
     * @private
     * @type {?string}
     */
    this.crossOrigin_ = crossOrigin;

    /**
     * Image URI
     *
     * @private
     * @type {string}
     */
    this.src_ = src;

    /**
     * @private
     * @type {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement}
     */
    this.image_ = document.createElement('video')
    this.image_.preload = 'auto'
    this.image_.autoplay = 'autoplay'
    this.image_.loop = 'loop'
    this.image_.type = 'video/mp4' // hack for now
    this.image_.width = '512'
    this.image_.height = '512'
    this.image_.crossOrigin = 'anonymous'

    if (crossOrigin !== null) {
      this.image_.crossOrigin = crossOrigin;
    }

    /**
     * @private
     * @type {Array<module:ol/events~EventsKey>}
     */
    this.imageListenerKeys_ = null;

    /**
     * @private
     * @type {module:ol/Tile~LoadFunction}
     */
    this.tileLoadFunction_ = tileLoadFunction;

  }

  if ( Tile ) VideoTile.__proto__ = Tile;
  VideoTile.prototype = Object.create( Tile && Tile.prototype );
  VideoTile.prototype.constructor = VideoTile;

  /**
   * @inheritDoc
   */
  VideoTile.prototype.disposeInternal = function disposeInternal () {
    if (this.state == TileState.LOADING) {
      this.unlistenImage_();
      this.image_ = getBlankImage();
    }
    if (this.interimTile) {
      this.interimTile.dispose();
    }
    this.state = TileState.ABORT;
    this.changed();
    Tile.prototype.disposeInternal.call(this);
  };

  /**
   * Get the HTML image element for this tile (may be a Canvas, Image, or Video).
   * @return {HTMLCanvasElement|HTMLImageElement|HTMLVideoElement} Image.
   * @api
   */
  VideoTile.prototype.getImage = function getImage () {
    return this.image_;
  };

  /**
   * @inheritDoc
   */
  VideoTile.prototype.getKey = function getKey () {
    return this.src_;
  };

  /**
   * Tracks loading or read errors.
   *
   * @private
   */
  VideoTile.prototype.handleImageError_ = function handleImageError_ () {
    this.state = TileState.ERROR;
    this.unlistenImage_();
    this.image_ = getBlankImage();
    this.changed();
  };

  /**
   * Tracks successful image load.
   *
   * @private
   */
  VideoTile.prototype.handleImageLoad_ = function handleImageLoad_ () {
    if (this.image_.videoWidth && this.image_.videoHeight) {
      this.image_.width = this.image_.videoWidth
      this.image_.height = this.image_.videoHeight
      this.video_ = this.image_
      this.image_ = document.createElement('img')
      this.image_.src = getBlankImage()
      this.state = TileState.LOADED;
      window.tiles = window.tiles || []
      window.tiles.push(this)
    } else {
      this.state = TileState.EMPTY;
    }
    this.unlistenImage_();
    this.changed();
  };

  /**
   * @inheritDoc
   * @api
   */
  VideoTile.prototype.load = function load () {
    if (this.state == TileState.ERROR) {
      this.state = TileState.IDLE;
      this.image_ = document.createElement('video')
      if (this.crossOrigin_ !== null) {
        this.image_.crossOrigin = this.crossOrigin_;
      }
    }
    if (this.state == TileState.IDLE) {
      this.state = TileState.LOADING;
      this.changed();
      this.imageListenerKeys_ = [
        listenOnce(this.image_, EventType.ERROR,
          this.handleImageError_, this),
        listenOnce(this.image_, 'canplaythrough',
          this.handleImageLoad_, this),
        listen(this.image_, 'loadedmetadata',
          this.syncer, this)
      ];
      this.tileLoadFunction_(this, this.src_);
    }
  };
  VideoTile.prototype.everyNFrame = function(n, fun) {
    let counter = 0;
    let breakout = false
    let f = () => {
      if (breakout) {
        return
      }
      if (counter % n == 0) {
        setTimeout(fun, 10)
      }      
      requestAnimationFrame(f)
    }
    f()
    return () => {
      breakout = true
    }
  }
  VideoTile.prototype.forceSync = function() {
    this.synced = false
    clearInterval(this.syncInterval)
    this.syncer()
  }
  VideoTile.prototype.syncer = function() {
    // this.syncInterval = setInterval(() => this.sync(), 1000)
    /*
    this.image_.addEventListener('timeupdate', () => {
      this.sync()
    })
    */
    setTimeout(() => {
      this.synced = false
      this.syncer()
    }, 30000)
    // this.syncInterval = this.everyNFrame(11, () => this.sync())
    this.loadedMetadata = true
  }
  VideoTile.prototype.sync = function() {
    if (this.synced) {
      // clearInterval(this.syncInterval);
      this.removeEventListener('timeupdate')
      // this.syncInterval()
      return;
    }
    const tolerance = 10 / 60 // 10 frames
    let video = this.getImage(); // get <video src="...">
    // other videos:
    const layer = this.layer
    const fps = layer.fps
    let videos = layer && layer.videos;
    if (videos && videos.length && video && video.buffered) {
      let timeRanges = video.buffered;
      // if we have it buffered?
      if (timeRanges && timeRanges.length) {
        let time = videos.map((v) => v.currentTime).reduce((a, t) => Math.max(a, t), 0)
        time = Math.round(time * fps) / fps
        let l = timeRanges.length;
        for (let i = 0; i < l; ++i) {
          // within buffered
          if (time >= timeRanges.start(i) && 
              time <= timeRanges.end(i)) {
            // signal synced
            this.synced = true;
            // seek only as little as possible
            // let roundedTime = Math.ceil(time)
            video.currentTime = time
            // videos.map(v => v.currentTime = roundedTime)
            // currentTime is/seems to be only hint
            this.layer.currentTime = time
            return;
          }
        }
      }
    }
  }

  /**
   * Discards event handlers which listen for load completion or errors.
   *
   * @private
   */
  VideoTile.prototype.unlistenImage_ = function unlistenImage_ () {
    this.imageListenerKeys_.forEach(unlistenByKey);
    this.imageListenerKeys_ = null;
  };

  return VideoTile;
}(Tile));
class VideoTileLayer extends TileLayer {
  constructor(options) {
    super(options);
    this.videos = [];
    this.fps = 1 //this.getSource().params.fps
    console.log(this.fps)
    var that = this;
    var source = this.getSource();
    var tileGrid = source.getTileGrid();
    const proj = this.getSource().getProjection()
    let lastExtent = null
    let postCompose = function (event) {
      var frameState = event.frameState;
      var extent = frameState.extent;
      let extentString = extent.toString()
      var ctx = event.context;
      var tileSize = source.tileGrid.getTileSize();
      var halfSizeWidth = tileSize[0] / 2;
      var halfSizeHeight = tileSize[1] / 2;
      that.videos = [];
      ctx.save();
      ctx.scale(frameState.pixelRatio, frameState.pixelRatio);
      let updateSync = extentString != lastExtent
      let once = true
      let z = tileGrid.getZForResolution(frameState.viewState.resolution)
      let fraction = frameState.viewState.zoom % 1
      let zRounded = z + fraction
      tileGrid.forEachTileCoord(extent, zRounded, function (tileCoord) {
        var tileCoordCenter = source.tileGrid.getTileCoordCenter(tileCoord);
        var middlePixel = map.getPixelFromCoordinate(tileCoordCenter);
        var topLeft = coordinate.add(middlePixel, [-halfSizeWidth, -halfSizeHeight]);
        var tile = source.getTile(tileCoord[0], tileCoord[1], tileCoord[2], 1, proj);
        tile.layer = that;
        if (tile.state !== TileState.LOADED) {
          return;
        }
        var video = tile.video_
        that.videos.push(video);
        that.draw(ctx, video, topLeft)
        if (updateSync) {
          tile.forceSync()
        }
      });
      ctx.restore();
      window.videos = that.videos;
      lastExtent = extentString
    };
    this.on('postcompose', postCompose);
    this.postCompose = postCompose
  }
  draw(ctxTo, video, topLeft) {
    var canvas, ctx
    if (!video.canvas) {
      canvas = video.canvas = document.createElement('canvas')
      canvas.width = video.videoWidth
      canvas.height = video.videoHeight
      ctx = video.ctx = canvas.getContext('2d')
    } else {
      canvas = video.canvas
      ctx = video.ctx
    }
    ctx.drawImage(video, 0, 0);
    // img.style.display = 'none';
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var data = imageData.data;
    const thresh = 8
    let maxGreen = 0
    for (let i = 0; i < data.length; i += 4) {
      maxGreen = data[i + 1] > maxGreen ? data[i + 1] : maxGreen
      //data[i + 3] = data[i] <= thresh && data[i + 1] <= thresh && data[i + 2] <= thresh ? 0 : data[i + 3]
      data[i + 3] = 255 - data[i + 1] // <= thresh && data[i + 1] <= thresh && data[i + 2] <= thresh ? 0 : data[i + 3]
      data[i + 1] = 0
      //data[i + 3] = 0
    }
    //console.log(`maxGreen`, maxGreen)
    ctx.putImageData(imageData, 0, 0)
    // ctxTo.drawImage(video, topLeft[0], topLeft[1]);
    // ctxTo.clearRect(topLeft[0], topLeft[1], 512, 512)
    ctxTo.drawImage(canvas, topLeft[0], topLeft[1]);
  }
}
class VideoTileWMS extends TileWMS {
  constructor(options) {
    options.format = options.format || 'video/mp4';
    // options.params = options.params || {}
    // options.params.fps = options.fps
    console.log(options)
    // options.timeExtent = options.
    options.tileClass = options.tileClass || VideoTile
    super(options);
    this.fps = options.fps
  }
}

proj4.defs('EPSG:3031', '+proj=stere +lat_0=-90 +lat_ts=-71 +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs'); register(proj4);

const projection = new Projection({
  code: 'EPSG:3031',
  extent: [-40030154.7, -40030154.7, 40030154.7, 40030154.7]
});
const resolutions = [9772.986997677057, 4886.4934988385285, 2443.2467494192642, 1221.6233747096321, 610.8116873548161, 305.40584367740803, 152.70292183870401, 76.35146091935201, 38.175730459676004, 19.087865229838002, 9.543932614919001];
const matrixIds = [4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
const options = {
  url: 'https://maps.environments.aq/mapcache/antarc/wmts',
  layer: 'antarc_add_coast',
  format: 'image/png',
  projection,
  matrixSet: 'antarc3031',
  extent: projection.getExtent(),
  tileGrid: new WMTSTileGrid({
    origin: [-40030154.7, 40030154.7],
    resolutions,
    matrixIds
  })
};

const wmtsLayer = new TileLayer({
  opacity: 1,
  source: new WMTS(options)
});
const videoSource = new VideoTileWMS({
  url: 'http://localhost:3000/wms-v',
  params: {
    layers: 'antarctica',
    map: '/opt/mapserver/mapfiles/antarctica_gen.map',
    interval: '2015-10-15T12:00:00.000Z/2015-11-15T12:00:00.000Z/P1D',
    url: 'http://localhost:3000/wmst',
    vfilter: 'blend',
    vfilter_time: '2'
  },
  tileGrid: new TileGrid({
    extent: projection.getExtent(),
    resolutions,
    projection,
    tileSize: [512, 512]
  }),
  ratio: 1,
  serverType: 'mapserver',
  projection
})
window.videoSource = videoSource
const wmstSource = new VideoTileWMS({
  url: 'http://localhost:3000/wmst',
  params: {
    layers: 'antarctica',
    map: '/opt/mapserver/mapfiles/antarctica_gen.map',
    time: '2015-01-01T11:00:00'
  },
  tileGrid: new TileGrid({
    extent: projection.getExtent(),
    resolutions,
    tileSize: [512, 512]
  }),
  ratio: 1,
  serverType: 'mapserver'
})
const wmsLayer = new VideoTileLayer({
  extent: projection.getExtent(),
  source: videoSource
});
/*
const debugLayer = new TileLayer({
  source: new TileDebug({
    projection,
    tileGrid: new TileGrid({
      extent: projection.getExtent(),
      resolutions,
      projection,
      tileSize: [512, 512]
    })
  })
})
*/
const map = new olMap({
  layers: [
    wmtsLayer,
    wmsLayer/*,
    debugLayer*/
  ],
  target: 'map',
  controls: defaultControls({
    attributionOptions: {
      collapsible: false
    }
  }),
  view: new olView({
    projection,
    center: [0, 0],
    zoom: 6
  })
});
const view = map.getView()
window.projection = projection
map.on('postcompose', (event) => {
  // console.log(event.frameState)
  // wmsLayer.postCompose(event)
  wmsLayer.dispatchEvent(event)
})
window.map = map
const frame = 1000 / 25
const halfFrame = frame / 2
let start = true
map.on("dragend", () => start = true)
setInterval(() => {
  map.render()
}, frame)
setTimeout(() => {
  setInterval(sync, frame)
}, halfFrame)
/*
setInterval(() => {
}, 100)
*/
// console.log(view.on)
// console.log(size, projection);
// https://maps.environments.aq/mapcache/antarc/wmts/?layer=antarc_add_coast&
// style=default&
// tilematrixset=antarc3031&
// Service=WMTS&
// Request=GetTile&
// Version=1.0.0&
// Format=image%2Fpng&
// TileMatrix=antarc3031%3A0&
// TileCol=0&
// TileRow=0

// https://maps.environments.aq/mapcache/antarc/wmts/?layer=antarc_ramp_bath_shade_mask&
// tilematrixset=antarc3031&
// Service=WMTS&
// Request=GetTile&
// Version=1.0.0&
// Format=png8&
// TileMatrix=5&
// TileCol=30&
// TileRow=31

// let currentTime = 0
let videoLength = Number.MAX_VALUE
let currentTime = 0
let lastTime = Date.now()
function sync() {
  let time = Date.now()
  let diff = time - lastTime
  lastTime = time
  let timeDiff = diff / 1000;
  currentTime += timeDiff
  let targetVideoTime = currentTime % videoLength
  // return console.log(arguments)

  /*
  if (this.synced) {
    clearInterval(this.syncInterval);
    return;
  }
  */
  
  // let video = this.getImage();
  let videos = wmsLayer.videos;
  if (videos && videos.length) {
    /*
    const fastSeekSupported = !!videos[0].fastSeek
    videos.map(v => {
      // v.preload = 'metadata'      
    })
    let data = videos.reduce((a, video) => {
      let minBuffered = a.buffered
      let duration = video.duration > 0 ? video.duration : 0
      a.duration = duration < a.duration ? duration : a.duration
      let timeRanges = video.buffered;
      let l = timeRanges && timeRanges.length;
      if (l) {
        for (let i = 0; i < l; ++i) {
          let end = timeRanges.end(i)
          minBuffered = end < minBuffered ? end : minBuffered
        }
      }
      a.buffered = minBuffered
      return a
    }, {
      buffered: Number.MAX_VALUE,
      duration: Number.MAX_VALUE
    })
    videoLength = data.duration
    if (targetVideoTime > data.buffered) {
      // targetVideoTime = data.buffered
    }
    if (videos.reduce((a, v) => a && v.buffered), true) {
      videos.map(v => v.paused && v.play())
      // videos.map(v => v.play())
    }
    // console.log(targetVideoTime)
    let c = videos[0].currentTime
    */

    if (videos.reduce((a, v) => a && v.buffered, true) && start) {
      start = false
      videos.map(v => {
        //v.currentTime = 0 // targetVideoTime
        // v.fastSeek && v.fastSeek(targetVideoTime) || v.c
        // fastSeekSupported && v.fastSeek(targetVideoTime) || (v.currentTime = targetVideoTime)
      })
      videos.map(v => {
        //v.play()
      })
    }
  }
  // requestAnimationFrame(sync)
  // setTimeout(sync, 1000)
}
// sync()
// setInterval(sync, 2000)
setInterval(() => {
  // videos.map(v => v.synced = false)
}, 2000)
window.syncVideos = function() {
  let c = videos && videos.length && videos.slice(1).reduce((a, v) => {
    let currentTime = v.currentTime
    return a < currentTime ? a : currentTime
  }, videos[0].currentTime)
  if (!c) {
    return
  }
  videos.map(v => v.currentTime = c)
}

map.on('moveend', () => {
  setTimeout(syncVideos, 1000)
})

const playBtn = document.querySelector('.control.play')
const pauseBtn = document.querySelector('.control.pause')
const syncBtn = document.querySelector('.control.sync')

syncBtn.addEventListener('click', syncVideos)
playBtn.addEventListener('click', () => {
  syncVideos()
  videos.map(v => v.play())
})
pauseBtn.addEventListener('click', () => {
  syncVideos()
  videos.map(v => v.pause())
})
/*
const t = videoSource.getTileGridForProjection(projection)
const ranges = t.fullTileRanges_
const tileUrlFunction = videoSource.getTileUrlFunction()
const tileUrls = []
ranges.slice(0, 1).map((r, z) => {
  // console.log(r.minX, r.minY, r.maxX, r.maxY)
  a: for (let x = r.minX; x <= r.maxX; ++x) {
    for (let y = r.minY; y <= r.maxY; ++y) {
      if (tileUrls.length > 1000) {
        break a
      }
      tileUrls.push(tileUrlFunction([x, y, z], 1, projection))
    }
  }
})
window.tileUrls = tileUrls
window.ranges = ranges
*/
// console.log(tileUrls)
// console.log(videoSource.tileUrlFunction(t.fullTileRanges_[0], 1, projection))
const tileUrlFunction = videoSource.getTileUrlFunction();
map.on('click', function(event) {
  const grid = videoSource.getTileGrid();
  const tileCord = grid.getTileCoordForCoordAndZ(event.coordinate, view.getZoom());
  console.log('clicked ', event.coordinate[0], event.coordinate[1]);
  console.log('tile z,x,y is:', tileCord[0], tileCord[1], tileCord[2]);
  console.log(tileUrlFunction(tileCord, 1, projection));
})