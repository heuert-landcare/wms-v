const watch = require("watch")
const exec = require("child_process").exec
// const promisify = require("util").promisify
const folder = process.argv[2]
const target = process.argv[3]
const excludeFile = process.argv[4]
let inprogress = false
watch.watchTree(folder, () => {
    if (!inprogress) {
        inprogress = true
        const p = exec(`rsync -avp --exclude=.git --exclude-from=${excludeFile} --progress ${folder} ${target}`)
        p.stderr.pipe(process.stderr)
        p.stdout.pipe(process.stdout)
        p.once("close", () => inprogress = false)
    }
})