# wms-v

WMS-V Proof of concept repository.

## FOSS4G Abstract

### WMS-V - Video Map Tiles for the 3rd or 4th Dimension

WMS-V, Web Mapping Service - Videos were presented at FOSS4G 2013 in Nottingham as a proof-of-concept.

We are using Video Map Tiles to visualise a model on possum population density and growth prediction over time.

Since then it has been developed by Manaaki Whenua - Landcare Research into a dynamic video tiling
service with modern technology such as ExpressJS and TypeScript. As a standard that I would like to see proposed it has not made much
progress, but in terms of technology and possibilities, faster compute and client performance the case is becoming a more viable proposition. In fact the video encoding can now be done on the fly, it is not a bottle neck and tiles can more transparently be synchronised on the client.

Join me on my journey with longitudinal compression, its benefits and also its challenges.