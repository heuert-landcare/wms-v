const request = require("request")
const fs = require("fs")
const util = require("./util")
const tenMinutes = 10 * 60 * 1000
const oneHour = 60 * 60 * 1000
const template = `http://rammb.cira.colostate.edu/ramsdis/online/images/hi_res/himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_{{time}}.jpg`


const folder = "backwards"
if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder)
}
const now = new Date().getTime()
let thenTime = Math.floor(now / tenMinutes) * tenMinutes - oneHour
let cont = true
function download() {
    const date = new Date(thenTime)
    const time = util.getFileSubTime(date)
    const filename = `${folder}/${time}.jpg`
    const writer = fs.createWriteStream(filename)
    // console.log(url.substring(url.length - 20))
    const url = template.replace("{{time}}", time)
    const r = request(url)
    r.pipe(writer)
    r.once("end", () => {
        console.log(`finished`)
        thenTime -= tenMinutes
        if (cont) {
            download()
        }
    })
    r.once("error", (err) => {
        console.log("error:", err)
        cont = false
    })
    console.log(`downloading ${filename} from ${url}`)
}
download()