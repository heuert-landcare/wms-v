const execSync = require("child_process").execSync
const fs = require("fs")

const l = "originals/full_disk_ahi_true_color_20181010111000.jpg".length
const dl = "20181010111000".length
const jl = ".jpg".length

function getFolder(time, z) {
    const zs = `${z}`.padStart(2, "0")
    return `pyramid/${time}/${zs}/cropped`
}
const wd = process.cwd()
function generateVideo(firstFile) {
    const parts = firstFile.split("/")
    const time = parts[1]
    const rest = parts.slice(2).join("/")
    const times = fs.readdirSync("pyramid", { encoding: "utf8" })
    const outfileBase = rest.replace("cropped/", "").substring(0, rest.length - 4)
    const txtFile = `${wd}/himawari_inputs_${outfileBase.replace(/\//g, "_")}.txt`
    const movieFile = outfileBase.replace(".jpg", "") + ".mp4"
    const outputFolder = `/opt/data/ssd/himawari/pyramid`
    if (!fs.existsSync(outputFolder)) {
        fs.mkdirSync(outputFolder)
        fs.mkdirSync(`${outputFolder}/movies`)
    }
    const inputs = times.map(time => `pyramid/${time}/${rest}`).sort()
    const ws = fs.createWriteStream(txtFile, { encoding: "utf8" })
    //console.log(time, rest)
    // inputs.forEach(i => console.log(i))
    ws.on("close", () => {
        const outputFile = `${outputFolder}/movies/${movieFile}`
        const parts = outputFile.split("/")
        const outputDir = parts.slice(0, parts.length - 1).join("/")
        execSync(`mkdir -p ${outputDir}`)
        const command = `~/projects/landcare/wms-v/ffmpeg-4.0.1/ffmpeg -f concat -i ${txtFile} -c:v libx264 ${outputFile}`
        console.log(command)
    })
    inputs.forEach(input => ws.write(`file ${input}\n`))
    ws.close()
    // ~/projects/landcare/wms-v/ffmpeg-4.0.1/ffmpeg -framerate 12 -pattern_type glob -i 'originals/*.jpg' -c:v libx264 -pix_fmt yuv420p out.mp4
}
function makeMovies(times) {
    const frames = times.map(time => {
        for (let z = 0; z < 10; ++z) {
            let zs = `${z}`.padStart(2, "0")
            let folder = `pyramid/${time}/${zs}/cropped`
            let files = fs.readdirSync(folder, { encoding: "utf8" })
            let paths = files.map(file => `${folder}/${file}`)
            paths.forEach(generateVideo)
        }
    })
}
function getTime(image) {
    return image.substring(l - (dl + jl), l - jl)
}
function task() {
    const out = execSync("ls originals", { encoding: "utf8"})
    // console.log(out)
    const images = out.split("\n").filter(i => i != "").map(i => `originals/${i}`)
    // console.log(images)
    const times = images.map(getTime).slice(0, 2)
    makeMovies(times)
}

task()