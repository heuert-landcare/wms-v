const execSync = require("child_process").execSync
const request = require('request')
const progress = require('request-progress')
const logUpdate = require('log-update')
// const promisify = require("util").promisify
const fs = require("fs")
// const x = promisify(exec)
// const url = `http://rammb.cira.colostate.edu/ramsdis/online/images/hi_res/himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_20181106034000.jpg`
// const url = `http://rammb.cira.colostate.edu/ramsdis/online/images/hi_res/
// himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_20181106034000.jpg`
// http://rammb.cira.colostate.edu/ramsdis/online/images/hi_res/himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_20181009062000.jpg
const template = `http://rammb.cira.colostate.edu/ramsdis/online/images/hi_res/himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_{{time}}.jpg`
//request(url).pipe(ws)
// const parts = url.split("/")
// const filename = parts[parts.length - 1]
// exec(`wget ${url} -O data/${filename}`)
//
function withProgress(url, to) {
	const promise = new Promise((resolve, reject) => {
		progress(request(url))
			.on('progress', function (state) {
				// The state is an object that looks like this:
				// {
				//     percent: 0.5,               // Overall percent (between 0 to 1)
				//     speed: 554732,              // The download speed in bytes/sec
				//     size: {
				//         total: 90044871,        // The total payload size in bytes
				//         transferred: 27610959   // The transferred payload size in bytes
				//     },
				//     time: {
				//         elapsed: 36.235,        // The total elapsed seconds since the start (3 decimals)
				//         remaining: 81.403       // The remaining seconds to finish (3 decimals)
				//     }
				// }
				logUpdate(`progress: ` + `${Math.round(state.percent * 100)}%`.padStart(3, " ") + ` of ${state.size.total} ETA: ${state.time.remaining} seconds`);
			})
			.on('error', function (err) {
				// Do something with err
				console.error(err)
			})
			.on('end', function () {
				// Do something after request finishes
				resolve(url)
			})
			.pipe(fs.createWriteStream(to))
	})
	return promise
}
async function download() {
    const start = new Date("2018-10-11T08:50:00.000Z")
    const end = new Date("2018-11-08T22:00:00.000Z")
	const now = Date.now()
	const tenMinutes = 10*60*1000
	const to10 = Math.floor(now / tenMinutes) * tenMinutes

	let date = new Date(to10 - (end.getTime() - start.getTime()))
	const nowEnd = new Date(to10)
	
    while (date.getTime() <= nowEnd.getTime()) {
        let iso = date.toISOString()
        let time = iso.replace("T", " ").substring(0, 19).replace(/[: -]/g, "")
        let url = template.replace("{{time}}", `${time}`)
        let filename = `data/${time}.jpg`
		if (fs.existsSync(filename) && fs.statSync(filename).size !== 0) {
			console.log(`skipping ${filename}...`)
			date = new Date(date.getTime() + 10 * 60 * 1000)
            continue
        }
        // let o = await x(`wget "${url}" -O ${filename}`)
        // execSync(`wget "${url}" -O ${filename}`)
		let u = await withProgress(url, filename)
        console.log(`downloaded ${filename} from ${u}`)
		console.log(`Waiting...`)
		// console.log(filename)
        // console.log(out)
        //console.log(time)
        date = new Date(date.getTime() + 10 * 60 * 1000)
    }
}
download()
