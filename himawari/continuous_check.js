const fs = require("fs")

const timeDiff = (data) => {
	const {
		start,
		end
	} = data
	return end.getTime() - start.getTime()
}
const getLongestSequence = (ls) => {
	const times = ls
		.map(t => `${t.substring(0, 4)}-${t.substring(4, 6)}-${t.substring(6, 8)}T` +
			`${t.substring(8, 10)}:${t.substring(10, 12)}:00.000Z`)
	const dates = times.map(t => new Date(t))
	const tenMinutes = 10 * 60 * 1000
	const interval = {
		longest: {
			start: dates[0],
			end: dates[0]
		},
		current: {
			start: dates[0],
			end: dates[0]
		}
	}
	const data = dates.slice(1).reduce((a, date) => {
		if (a.current.end.getTime() == (date.getTime() - tenMinutes)) {
			a.current.end = date
		} else {
			a.current = {
				start: date,
				end: date
			}
		}
		if (timeDiff(a.current) > timeDiff(a.longest)) {
			a.longest = a.current
		}
		return a
	}, interval)
	const {
		longest
	} = data
	return longest
}
module.exports = {
	timeDiff,
	getHoles: function () {
		const ls = fs.readdirSync("data")
		const times = ls.map(tj => tj.substring(0, tj.indexOf(".jpg")))
			.map(t => `${t.substring(0, 4)}-${t.substring(4, 6)}-${t.substring(6, 8)}T${t.substring(8, 10)}:${t.substring(10, 12)}`)
		const dates = times.map(t => new Date(t))
		let lastDate = null
		const tenMinutes = 10 * 60 * 1000
		for (let date of dates) {
			if (lastDate != null && date.getTime() > lastDate.getTime() + tenMinutes) {

			}
			lastDate = date
		}
		const holes = dates.reduce((a, date) => {
			if (a.last != null && date.getTime() > a.last.getTime() + tenMinutes) {
				a.holes.push({
					start: a.last,
					end: date
				})
			}
			a.last = date
			return a
		}, {
			holes: [],
			last: null
		})
		// console.log(holes.holes)
		return holes
	},
	getLongestSequence
}


function test() {
	const seq = getLongestSequence([
		"20181108215000",
		"20181108220000",
		"20181108221000",
		"20181108222000",
		"20181108223000",
		"20181108224000",
		"20181108225000",

		"20181108235000",
		"20181108240000",
		"20181108241000",
	])
	seq.start.toISOString() //?
	seq.end.toISOString() //?
}
// test()