const fs = require("fs")
const util = require("./util")
const folder = "data"// process.argv[2]
const files = fs.readdirSync(folder).map(f => f.substring(0, f.length - 4)).map(util.mkTime)
const gaps = util.findGaps(files, (a, b) => {
    const tenMinutes = 10 * 60 * 1000
    return b.getTime() - tenMinutes !== a.getTime()
})
const min = gaps.reduce((a, d) => {
    const diff = d.end.getTime() - d.start.getTime()
    return diff < a ? diff : a
}, Number.MAX_VALUE)
gaps.forEach(g => console.log(`start: ${g.start.toISOString()} end: ${g.end.toISOString()}`))