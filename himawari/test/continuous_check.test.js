const check = require("../continuous_check")
const expect = require("expect")
const timeDiff = check.timeDiff
describe("check", () => {
	describe("check.timeDiff()", () => {
		it("should find the right time difference", () => {
			const data = {
				start: new Date("2018-11-09T21:00:00.000Z"),
				end: new Date("2018-11-09T22:00:00.000Z")
			}
			const diff = timeDiff(data)
			expect(diff).toBe(60 * 60 * 1000)
		})
	})
	describe("check.getLongestSequence()", () => {
		it("should find the first", () => {
			// expect([1,2,3].indexOf(4)).toBe(-1)
			const seq = check.getLongestSequence([
				"20181108215000",
				"20181108220000",
				"20181108221000",
				"20181108222000",
				"20181108223000",
				"20181108224000",
				"20181108225000",

				"20181108235000",
				"20181108240000",
				"20181108241000",
			])
			expect(seq).toEqual({
				start: new Date("2018-11-08T21:50:00.000Z"),
				end: new Date("2018-11-08T22:50:00.000Z")
			})
		});
		it("should find the last", () => {
			// expect([1,2,3].indexOf(4)).toBe(-1)
			const seq = check.getLongestSequence([
				"20181108215000",
				"20181108220000",
				"20181108221000",

				"20181108223000",
				"20181108224000",
				"20181108225000",
				"20181108230000",
				"20181108231000",
				"20181108232000",
			])
			expect(seq).toEqual({
				start: new Date("2018-11-08T22:30:00.000Z"),
				end: new Date("2018-11-08T23:20:00.000Z")
			})
		});
	});
});

if(typeof global.runQuokkaMochaBdd === 'function') {
    runQuokkaMochaBdd();
}