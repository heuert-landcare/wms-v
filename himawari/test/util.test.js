const util = require("../util")
const expect = require("expect")
describe("util", () => {
	describe("makeShort()", () => {
		it("should shorten a date", () => {
            const short = util.makeShort(new Date("2018-11-01T12:00:00.000Z"))
            expect(short).toBe("20181101120000")
        })
    })
    describe("shortDates()", () => {
        it("should give 5 short dates", () => {
            const timeInterval = {
                start: new Date("2018-11-09T12:00:00.000Z"),
                end: new Date("2018-11-09T12:40:00.000Z")
            }
            const shortDates = util.shortDates(timeInterval)
            // shortDates
            expect(shortDates.length).toBe(5)
            expect(shortDates[1]).toBe("20181109121000")
        })
    })
    /*
(images, mkdir, checkPyramidExists, zoomMap, zooms) => {
	// zoomMap = z => `pyramid/${t}/${z}`
	// checkPyramidExists = () => fs.existsSync("pyramid")
	// zooms = ["00", "01","02","03","04"]
    // mkdir = f => execSync(`mkdir -p ${f}`)
    */
    describe("getTime()", () => {
        it("should get the time from an image", () => {
            const time = util.getTime("data/20181108234000.jpg")
            time //?
            expect(time).toBe("20181108234000")
        })
    })
    describe("makeFolders()", () => {
        const images = [
            "data/20181108234000.jpg",
            "data/20181108114000.jpg",
        ]
        const dirs = []
        const mockMkdir = f => dirs.push(f)
        const checkPyramidExists = () => false
        const zoomMap = time => (zoom => `pyramid/${time}/${zoom}`)
        const zooms = ["00", "01", "02"]
        util.makeFolders(images, mockMkdir, checkPyramidExists, zoomMap, zooms)
        expect(dirs).toEqual([
            "pyramid/20181108234000/00",
            "pyramid/20181108234000/01",
            "pyramid/20181108234000/02",
            "pyramid/20181108114000/00",
            "pyramid/20181108114000/01",
            "pyramid/20181108114000/02",
        ])
    })
    describe("mkdirR()", () => {
        const dirs = []
        const mkdir = d => dirs.push(d)
        const check = c => true
        util.mkdirR("/a/b/c", mkdir, check)
        expect(dirs).toEqual(["/", "/a", "/a/b", "/a/b/c" ])
    })
    describe("findGaps()", () => {
        const gaps = util.findGaps([1,2,4,5,7,11,12,13], (a, b) => a + 1 !== b)
        gaps //?
        expect(gaps).toEqual([{
                start: 2,
                end: 4
            },
            {
                start: 5,
                end: 7
            },
            {
                start: 7,
                end: 11
            }
        ])
    })
})

if(typeof global.runQuokkaMochaBdd === 'function') {
    runQuokkaMochaBdd();
}