// convert originals/full_disk_ahi_true_color_20181106200000.jpg -resize 50% -crop 500x500 +repage half/image_%03d.jpg
const execSync = require("child_process").execSync
const fs = require("fs")
// const promisify = require("util").promisify
// const x = promisify(exec)

const l = "originals/full_disk_ahi_true_color_20181010111000.jpg".length
const dl = "20181010111000".length
const jl = ".jpg".length

function getFolder(time, z) {
    const zs = `${z}`.padStart(2, "0")
    return `pyramid/${time}/${zs}/cropped`
}
function getTime(image) {
    return image.substring(l - (dl + jl), l - jl)
}
function makeFolders(images) {
    if (fs.existsSync("pyramid")) {
        return
    }
    const times = images.filter(t => t != '').map(getTime)
    times.forEach(t => {
        const zooms = ["00", "01","02","03","04","05","06","07","08","09"]
        const folders = zooms.map(z => `pyramid/${t}/${z}`)
        folders.forEach(f => execSync(`mkdir -p ${f}`))
    })
}
function resize(images) {
    // convert originals/full_disk_ahi_true_color_20181106200000.jpg -resize 50% -crop 500x500 +repage half/image_%03d.jpg
    images.forEach(image => {
        for (let z = 1; z <= 10; ++z) {
            let s = z * 10
            let time = getTime(image)
            let folder = getFolder(time, z - 1)
            if (!fs.existsSync(folder)) {
                fs.mkdirSync(folder, { recursive: true })
            }
                let command = `convert ${image} -resize ${s}% -crop 500x500 +repage ${folder}/%03d.jpg`
                let out = execSync(command, { encoding: "utf8" })
                console.log(command)
                console.log(out)
        }
    })
}

function task() {
    const out = execSync("ls originals", { encoding: "utf8"})
    // console.log(out)
    const images = out.split("\n").filter(i => i != "").map(i => `originals/${i}`)
    // console.log(images)
    makeFolders(images)
    resize(images)
}

task()