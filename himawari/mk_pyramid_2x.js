// convert originals/full_disk_ahi_true_color_20181106200000.jpg -resize 50% -crop 500x500 +repage half/image_%03d.jpg
const execSync = require("child_process").execSync
const fs = require("fs")
// const promisify = require("util").promisify
// const x = promisify(exec)

const l = "originals/full_disk_ahi_true_color_20181010111000.jpg".length
const dl = "20181010111000".length
const jl = ".jpg".length

function getFolder(time, z) {
    const zs = `${z}`.padStart(2, "0")
    return `pyramid_2x/${time}/${zs}/cropped`
}
function getTime(image) {
    return image.substring(l - (dl + jl), l - jl)
}
function makeFolders(images) {
    if (fs.existsSync("pyramid_2x")) {
        return
    }
    const times = images.filter(t => t != '').map(getTime)
    times.forEach(t => {
        const zooms = ["00", "01","02","03","04","05"]
        const folders = zooms.map(z => `pyramid_2x/${t}/${z}`)
        folders.forEach(f => execSync(`mkdir -p ${f}`))
    })
}
function resize(images) {
    // convert originals/full_disk_ahi_true_color_20181106200000.jpg -resize 50% -crop 500x500 +repage half/image_%03d.jpg
    images.forEach(image => {
        const zooms = ["6.25", "12.5", "25", "50", "100"]
        for (let z = 1; z < 6; ++z) {
            let s = zooms[z - 1]
            let time = getTime(image)
            let folder = getFolder(time, z - 1)
            if (!fs.existsSync(folder)) {
                fs.mkdirSync(folder, { recursive: true })
            }
            let command = `convert ${image} -resize ${s}% -crop 512x512 +repage ${folder}/%03d.jpg`
            // let out = execSync(command, { encoding: "utf8" })
            console.log(command)
            // console.log(out)
        }
    })
}

function task() {
    // const out = execSync("ls originals", { encoding: "utf8"})
    const ls = fs.readdirSync("originals")
    // console.log(out)
    // const images = out.split("\n").filter(i => i != "").map(i => `originals/${i}`)
    const images = ls.map(i => `originals/${i}`)
    // console.log(images)
    makeFolders(images)
    resize(images)
}

task()