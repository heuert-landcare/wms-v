const l = "data/full_disk_ahi_true_color_20181010111000.jpg".length
const dl = "20181010111000".length
const jl = ".jpg".length

const mkdirR = (dir, mk = d => e, check = d => e, done = 0) => {
	const parts = dir.split("/")
	if (done === parts.length) {
		return
	}
	const doneDirs = parts.slice(0, done)
	const rest = parts.slice(done)
    const dirToMk = doneDirs.join("/") + "/" + rest[0]
    if (check(dirToMk)) {
        mk(dirToMk)
    }
	mkdirR(dir, mk, check, done + 1)
}

const findGaps = (ar, checkGap) => {
    return ar.reduce((a, v) => {
        if (a.last && checkGap(a.last, v)) {
            a.gaps.push({
                start: a.last,
                end: v
            })
        }
        a.last = v
        return a
    }, {
       gaps: [],
       last: false
    }).gaps
}
// const gaps = findGaps([1, 2, 4, 5, 7, 11, 12, 13], (a, b) => a + 1 !== b)
// gaps //?
const makeShort = (long) => {
    const l = long.toISOString()
    const s = l.substring.bind(l)
    return s(0, 4) + s(5, 7) + s(8, 10) + s(11, 13) + s(14, 16) + s(17, 19)
}

const getTime = (image) => {
    const parts = image.split(/[.\/]/)
	return parts[1] // image.substring(l - (dl + jl), l - jl)
}

const shortDates = (timeInterval) => {
    const {start, end} = timeInterval
    const tenMinutes = 10 * 60 * 1000
    const shorts = []
    for (let time = start.getTime(); time <= end.getTime(); time += tenMinutes) {
        shorts.push(new Date(time))
    }
    return shorts.map(makeShort)
}

const makeFolders = (images, mkdir, checkPyramidExists, zoomMap, zooms) => {
	// zoomMap = z => `pyramid/${t}/${z}`
	// checkPyramidExists = () => fs.existsSync("pyramid")
	// zooms = ["00", "01","02","03","04"]
	// mkdir = f => execSync(`mkdir -p ${f}`)
	if (checkPyramidExists()) {
		return
	}
	const times = images.map(getTime)
	times.forEach(time => {
		// const zooms = ["00", "01","02","03","04","05","06","07","08","09"]
		const folders = zooms.map(zoomMap(time))
		// folders.forEach(f => execSync(`mkdir -p ${f}`))
		folders.forEach(mkdir)
	})
}
const getFileSubTime = (date) => {
    let iso = date.toISOString()
    let time = iso.replace("T", " ").substring(0, 19).replace(/[: -]/g, "")
    return time
}
const mkTime = (timeShort) => {
    const t = timeShort
	const ts = `${t.substring(0, 4)}-${t.substring(4, 6)}-${t.substring(6, 8)}T` +
			`${t.substring(8, 10)}:${t.substring(10, 12)}:00.000Z`
    return new Date(ts)
}
// mkTime("20181123145900").toISOString() //?
module.exports = {
    makeShort,
    shortDates,
    makeFolders,
    getTime,
    getFileSubTime,
    mkdirR,
    findGaps,
    mkTime
}
/*
const shorts = shortDates({
    start: new Date("2018-11-01T12:00:00Z"),
    end: new Date("2018-12-01")
})
shorts[0] //?
shorts[shorts.length - 1] //?

makeShort(shorts[0]) //?
*/