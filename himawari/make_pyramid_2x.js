// convert originals/full_disk_ahi_true_color_20181106200000.jpg -resize 50% -crop 500x500 +repage half/image_%03d.jpg
const execSync = require("child_process").execSync
const fs = require("fs")
const checkUtil = require("./continuous_check")
const util = require("./util")
const getTime = util.getTime

function getFolder(time, z) {
	const zs = `${z}`.padStart(2, "0")
	return `pyramid/${time}/${zs}/cropped`
}
function resize(images) {
	// convert originals/full_disk_ahi_true_color_20181106200000.jpg -resize 50% -crop 500x500 +repage half/image_%03d.jpg
	const zooms = [`6.25`, `12.5`, `25`, `50`, `100`]
	images.forEach(image => {
		zooms.forEach(z => {
			let time = getTime(image)
			let folder = getFolder(time, z - 1)
			if (!fs.existsSync(folder)) {
				fs.mkdirSync(folder, { recursive: true })
			}
			let command = `convert ${image} -resize ${z}% -crop 512x512 +repage ${folder}/%03d.jpg`
			console.log(command)
			execSync(command, { encoding: "utf8" })
			console.log(`finished`)
		})
	})
}

function task() {
	const ls = fs.readdirSync("data")
	const longest = checkUtil.getLongestSequence(ls.map(tj => tj.substring(0, tj.indexOf(".jpg"))))
	const names = util.shortDates(longest)
	// names
	const images = names.map(i => `data/${i}.jpg`)
	// const folders = []
	const check = d => !fs.existsSync(d)
	const mkdir = f => util.mkdirR(f, fs.mkdirSync, check)
	const checkPyramidExists = () => fs.existsSync("pyramid")
	const zoomMap = time => (zoom => `pyramid/${time}/${zoom}`)
	const zooms = ["00", "01", "02", "03", "04"]
	// checkPyramidExists() //?
	// images.length //?
	//images
	// console.log(images)
	//makeFolders(images)
	// images[4] //?
	util.makeFolders(images, mkdir, checkPyramidExists, zoomMap, zooms)
	// folders.slice(0, 5) //?
	//resize(images)
	console.log(`All finished!`)
}

task()
// const ls = fs.readdirSync("data")
// const longest = check.getLongestSequence(ls.map(tj => tj.substring(0, tj.indexOf(".jpg"))))
// console.log(longest)
