const _ = require("lodash")
const fs = require("fs")
// const folder = process.argv && process.argv[2] || "pyramid_2x"
const folder = "pyramid_2x"
const moviesFolder = "movies_2x"
const folders = fs.readdirSync(folder)
const zooms = [...Array(5).keys()] //?
const zoomss = zooms.map(z => `${z}`.padStart(2, "0")) //?
const powers = [1, 4, 9, 36, 121] //zooms.map(z => (z + 1)**2) //?
const powersArrays = powers.map(p => [...Array(p).keys()]) //?
const getPadded = (toPad) => `${toPad}`.padStart(3, "0")
// 00/000
// 01/000 01/001 01/002 01/003
// 02/000 02/001 02/002 01
const differentFiles = zoomss.map((zs, i) => powersArrays[i].map(p => `${zs}/${getPadded(p)}.jpg`))
const inputs = _.flatMapDeep(differentFiles) // .slice(0, 10)
const all = inputs.map(input => folders.map(f => `${f}/${input}`))
// all[1] //?
const descriptors = all.map((ar, i) => ({
    file: `${folder}/${i}.in`,
    content: ar.map(f => `file ${f.substring(0, f.length - 7)}cropped/${f.substring(f.length - 7)}`).join("\n")
}))
descriptors.forEach(desc => fs.writeFileSync(desc.file, desc.content))
const commands = descriptors.map((desc, i) => {
    const file = all[i][0]
    const outputFile = moviesFolder + "/" + file.substring(0, file.length - 4) + ".mp4"
    // const outputFile = `out.mp4`
    return `ffmpeg -f concat -i ${desc.file} -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v libx264 ${outputFile}`
}) //?
// write .in files with image paths
// const command = `ffmpeg -f concat -i ${txtFile} -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v libx264 ${outputFile}`
// zoomss.forEach(zs => fs.mkdirSync(`${moviesFolder}/${zs}`))
folders.map(folder => {
	!fs.existsSync(`${moviesFolder}/${folder}`) && fs.mkdirSync(`${moviesFolder}/${folder}`)
	zoomss.forEach(zs => !fs.existsSync(`${moviesFolder}/${folder}/${zs}`) && fs.mkdirSync(`${moviesFolder}/${folder}/${zs}`))
})
commands.forEach(command => console.log(command))
