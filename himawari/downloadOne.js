const request = require("request")
const fs = require("fs")
const util = require("./util")
const tenMinutes = 10 * 60 * 1000
const oneHour = 60 * 60 * 1000
const now = new Date().getTime()
const uptoOneHourAndTenMinutesAgo = Math.floor(now / tenMinutes) * tenMinutes - oneHour
const template = `http://rammb.cira.colostate.edu/ramsdis/online/images/hi_res/himawari-8/full_disk_ahi_true_color/full_disk_ahi_true_color_{{time}}.jpg`
const date = new Date(uptoOneHourAndTenMinutesAgo)
const time = util.getFileSubTime(date)
const url = template.replace("{{time}}", time)
const folder = "every10"
if (!fs.existsSync(folder)) {
    fs.mkdirSync(folder)
}
const filename = `${folder}/${time}.jpg`
const writer = fs.createWriteStream(filename)
// console.log(url.substring(url.length - 20))
const r = request(url)
r.pipe(writer)
r.once("end", () => {
    console.log(`finished`)
})
console.log(`downloading ${filename} from ${url}`)