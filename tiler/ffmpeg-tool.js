"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var tiler_pyramider_1 = require("./tiler-pyramider");
var underscore_1 = __importDefault(require("underscore"));
function getArray(generator) {
    var result = [];
    for (var gen = generator(), next = gen.next(); !next.done; next = gen.next()) {
        result.push(next.value);
    }
    return result;
}
exports.getArray = getArray;
var FfmpegTool = /** @class */ (function () {
    function FfmpegTool(width, height, levels, sourceFilename, tileWidth, tileHeight) {
        if (sourceFilename === void 0) { sourceFilename = 'source.ext'; }
        if (tileWidth === void 0) { tileWidth = 256; }
        if (tileHeight === void 0) { tileHeight = 256; }
        this.width = width;
        this.height = height;
        this.levels = levels;
        this.sourceFilename = sourceFilename;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }
    FfmpegTool.prototype.tile = function (pyramid) {
        var _this = this;
        var tiles = pyramid.map(function (p, i) {
            return new tiler_pyramider_1.LevelTiler(p[0], p[1], pyramid.length - i - 1, _this.tileWidth, _this.tileHeight).get();
        });
        var result = tiles.map(function (ts) {
            return ts.map(function (t) {
                var zoom = t[0];
                var paddedZoom = ('' + zoom).padStart(6, '0');
                return new tiler_pyramider_1.TileCommand(t, paddedZoom + ".mp4", _this.getFilename(zoom, t[5], t[6])).get();
            });
        });
        return result.slice(0, -1).reverse();
    };
    FfmpegTool.prototype.commandGenerator = function () {
        var pyramider = new tiler_pyramider_1.Pyramider(this.width, this.height, this.levels);
        var pyramid = pyramider.get();
        var result = pyramid.map(function (p, i) {
            var zoom = pyramid.length - i;
            var zoomPadded = ('' + zoom).padStart(6, '0');
            return new tiler_pyramider_1.PyramidCommand(p[0], p[1], zoomPadded + ".mp4", zoom - 1).get();
        }).concat(underscore_1.default.flatten(this.tile([[this.width, this.height]].concat(pyramid))));
        return underscore_1.default.flatten(result);
    };
    FfmpegTool.prototype.getFilename = function (zoom, tileX, tileY, extension, padLength) {
        if (extension === void 0) { extension = 'mp4'; }
        if (padLength === void 0) { padLength = 6; }
        var z = ("" + zoom).padStart(padLength, '0');
        var x = ("" + tileX).padStart(padLength, '0');
        var y = ("" + tileY).padStart(padLength, '0');
        return z + "/" + y + "/" + x + "." + extension;
    };
    return FfmpegTool;
}());
exports.FfmpegTool = FfmpegTool;
