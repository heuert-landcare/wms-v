"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var assert = __importStar(require("assert"));
var tiler_pyramider_1 = require("../tiler-pyramider");
var ffmpeg_tool_1 = require("../ffmpeg-tool");
describe('tiler', function () {
    describe('get tile info', function () {
        it('works with most basic case of 4 tiles', function () {
            var tiles = new tiler_pyramider_1.LevelTiler(512, 512, 0, 256, 256).get();
            assert.deepEqual(tiles, [
                [0, 256, 256, 0, 0, 0, 0],
                [0, 256, 256, 256, 0, 1, 0],
                [0, 256, 256, 0, 256, 0, 1],
                [0, 256, 256, 256, 256, 1, 1],
            ]);
        });
        it('works with uneven split', function () {
            assert.deepEqual(new tiler_pyramider_1.LevelTiler(500, 500).get(), [
                [0, 256, 256, 0, 0, 0, 0],
                [0, 500 - 256, 256, 256, 0, 1, 0],
                [0, 256, 500 - 256, 0, 256, 0, 1],
                [0, 500 - 256, 500 - 256, 256, 256, 1, 1],
            ]);
        });
        it('works with 2 levels', function () {
            assert.deepEqual(new tiler_pyramider_1.LevelTiler(500, 500).get(), [
                [0, 256, 256, 0, 0, 0, 0],
                [0, 500 - 256, 256, 256, 0, 1, 0],
                [0, 256, 500 - 256, 0, 256, 0, 1],
                [0, 500 - 256, 500 - 256, 256, 256, 1, 1],
            ]);
        });
        it('pyramids image with width and height', function () {
            assert.deepEqual(new tiler_pyramider_1.Pyramider(1024, 1024, 3).get(), [
                [512, 512],
                [256, 256],
            ]);
        });
        it('pyramids image with other width and height', function () {
            assert.deepEqual(new tiler_pyramider_1.Pyramider(1000, 1000, 3).get(), [
                [500, 500],
                [250, 250],
            ]);
        });
        it('pads a string with 0002', function () {
            assert.equal(("" + 2).padStart(4, '0'), '0002');
        });
        it('gets correct filenames for zoom, tileX and tileY', function () {
            var tool = new ffmpeg_tool_1.FfmpegTool(0, 0, 0);
            assert.equal(tool.getFilename(2, 23, 512), '000002/000512/000023.mp4');
            assert.equal(tool.getFilename(22, 231, 5122), '000022/005122/000231.mp4');
            assert.equal(tool.getFilename(2, 23, 512, 'mov'), '000002/000512/000023.mov');
        });
        it('gets the ffmpeg filters and filenames', function () {
            // const tool = new FfmpegTool(1000, 1000, 3)
            // assert.deepEqual(tool.getScaleFilters(), [
            //     'scale=w=1000:h=1000',
            //     'scale=w=500:h=500',
            //     'scale=w=250:h=250',
            // ])
            // assert.deepEqual(tool.getCropFilters(), [
            //     'crop=w=256:h=256:x=0:y=0',
            //     'crop=w=232:h=256:x=256:y=0',
            //     'crop=w=256:h=232:x=0:y=256',
            //     'crop=w=232:h=232:x=256:y=256', 
            // ])
        });
        it('gets tile command', function () {
            var tileCommand = new tiler_pyramider_1.TileCommand([0, 34, 45, 56, 67]);
            assert.equal(tileCommand.get(), "ffmpeg  -i input.mp4 -vf 'crop=w=34:h=45:x=56:y=67'  output.mp4");
        });
        it('gets pyramid command', function () {
            var pyramidCommand = new tiler_pyramider_1.PyramidCommand(123, 234).get();
            assert.equal(pyramidCommand, "ffmpeg  -i 000000.mp4 -vf 'scale=123:234'  000000.mp4");
            assert.equal(new tiler_pyramider_1.PyramidCommand(123, 234, 'input1.mp4', 3).get(), "ffmpeg  -i input1.mp4 -vf 'scale=123:234'  000003.mp4");
        });
        it('generates all commands', function () {
            var tool = new ffmpeg_tool_1.FfmpegTool(1024, 1024, 3, 'source.mp4', 256, 256);
            var commands = tool.commandGenerator();
            var expected = [
                "ffmpeg  -i 000002.mp4 -vf 'scale=512:512'  000001.mp4",
                "ffmpeg  -i 000001.mp4 -vf 'scale=256:256'  000000.mp4",
                "ffmpeg  -i 000001.mp4 -vf 'crop=w=256:h=256:x=0:y=0'  000001/000000/000000.mp4",
                "ffmpeg  -i 000001.mp4 -vf 'crop=w=256:h=256:x=256:y=0'  000001/000000/000001.mp4",
                "ffmpeg  -i 000001.mp4 -vf 'crop=w=256:h=256:x=0:y=256'  000001/000001/000000.mp4",
                "ffmpeg  -i 000001.mp4 -vf 'crop=w=256:h=256:x=256:y=256'  000001/000001/000001.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=0:y=0'  000002/000000/000000.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=256:y=0'  000002/000000/000001.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=512:y=0'  000002/000000/000002.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=768:y=0'  000002/000000/000003.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=0:y=256'  000002/000001/000000.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=256:y=256'  000002/000001/000001.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=512:y=256'  000002/000001/000002.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=768:y=256'  000002/000001/000003.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=0:y=512'  000002/000002/000000.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=256:y=512'  000002/000002/000001.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=512:y=512'  000002/000002/000002.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=768:y=512'  000002/000002/000003.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=0:y=768'  000002/000003/000000.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=256:y=768'  000002/000003/000001.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=512:y=768'  000002/000003/000002.mp4",
                "ffmpeg  -i 000002.mp4 -vf 'crop=w=256:h=256:x=768:y=768'  000002/000003/000003.mp4"
            ];
            assert.deepEqual(commands, expected);
        });
    });
});
