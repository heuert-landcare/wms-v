export class LevelTiler {
    constructor(
        private width: number,
        private height: number,
        private zoom: number = 0,
        private tileWidth: number = 256,
        private tileHeight: number = 256,
    ) {}

    get() {
        const result = []
        for (
            let it = this.gen(), next = it.next();
            !next.done;
            next = it.next()
        ) {
            result.push(next.value)
        }
        return result
    }

    * gen() {
        let xx = this.getNumberOfHorizontalTiles()
        let yy = this.getNumberOfVerticalTiles()
        for (let y = 0; y < yy; ++y) {
            let row = []
            for (let x = 0; x < xx; ++x) {
                let w = Math.min(this.width - x * this.tileWidth, this.tileWidth)
                let h = Math.min(this.height - y * this.tileHeight, this.tileHeight)
                yield [this.zoom, w, h, x * this.tileWidth, y * this.tileHeight, x, y]
            }
        }
    }

    getNumberOfHorizontalTiles(): number {
        return this.width / this.tileWidth
    }

    getNumberOfVerticalTiles(): number {
        return this.height / this.tileHeight
    }
}

export class Pyramider {
    constructor(
        private width: number,
        private height: number,
        private levels: number,
    ) {}
    get() {
        const result = []
        for (
            let it = this.gen(), next = it.next();
            !next.done;
            next = it.next()
        ) {
            result.push(next.value)
        }
        return result
    }
    * gen() {
        for (let i = 1; i < this.levels; ++i) {
            yield [Math.floor(this.width / 2**i), Math.floor(this.height / 2**i)]
        }
    }
}

// export class TilePyramider {
//     constructor(
//         private tiler: LevelTiler,
//         private pyramider: Pyramider,
//     ) {}
// }

export class TileCommand {
    constructor(
        private tile: number[],
        private inputName: string = 'input.mp4',
        private outputName: string = 'output.mp4',
        private preinput: string = '',
        private preoutput: string = '',
    ) {}
    get() {
        const {tile, preinput, preoutput, inputName, outputName} = this
        // console.log(tile)
        return `ffmpeg ${preinput} -i ${inputName} -vf ` +
            `'crop=w=${tile[1]}:h=${tile[2]}:x=${tile[3]}:y=${tile[4]}' ` +
            `${preoutput} ${outputName}`
    }
}

export class PyramidCommand {
    constructor(
        private width: number,
        private height: number,
        private input: string = '0'.padStart(6, '0') + '.mp4',
        private zoom: number = 0,
        private preinput: string = '',
        private preoutput: string = '',
    ) {}
    get() {
        const {width, height, input, preinput, preoutput, zoom} = this
        const zoomPadded = ('' + zoom).padStart(6, '0')
        return `ffmpeg ${preinput} -i ${input} -vf 'scale=${width}:${height}' ${preoutput} ${zoomPadded}.mp4`
    }
}