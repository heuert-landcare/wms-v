"use strict";
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var LevelTiler = /** @class */ (function () {
    function LevelTiler(width, height, zoom, tileWidth, tileHeight) {
        if (zoom === void 0) { zoom = 0; }
        if (tileWidth === void 0) { tileWidth = 256; }
        if (tileHeight === void 0) { tileHeight = 256; }
        this.width = width;
        this.height = height;
        this.zoom = zoom;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }
    LevelTiler.prototype.get = function () {
        var result = [];
        for (var it_1 = this.gen(), next = it_1.next(); !next.done; next = it_1.next()) {
            result.push(next.value);
        }
        return result;
    };
    LevelTiler.prototype.gen = function () {
        var xx, yy, y, row, x, w, h;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    xx = this.getNumberOfHorizontalTiles();
                    yy = this.getNumberOfVerticalTiles();
                    y = 0;
                    _a.label = 1;
                case 1:
                    if (!(y < yy)) return [3 /*break*/, 6];
                    row = [];
                    x = 0;
                    _a.label = 2;
                case 2:
                    if (!(x < xx)) return [3 /*break*/, 5];
                    w = Math.min(this.width - x * this.tileWidth, this.tileWidth);
                    h = Math.min(this.height - y * this.tileHeight, this.tileHeight);
                    return [4 /*yield*/, [this.zoom, w, h, x * this.tileWidth, y * this.tileHeight, x, y]];
                case 3:
                    _a.sent();
                    _a.label = 4;
                case 4:
                    ++x;
                    return [3 /*break*/, 2];
                case 5:
                    ++y;
                    return [3 /*break*/, 1];
                case 6: return [2 /*return*/];
            }
        });
    };
    LevelTiler.prototype.getNumberOfHorizontalTiles = function () {
        return this.width / this.tileWidth;
    };
    LevelTiler.prototype.getNumberOfVerticalTiles = function () {
        return this.height / this.tileHeight;
    };
    return LevelTiler;
}());
exports.LevelTiler = LevelTiler;
var Pyramider = /** @class */ (function () {
    function Pyramider(width, height, levels) {
        this.width = width;
        this.height = height;
        this.levels = levels;
    }
    Pyramider.prototype.get = function () {
        var result = [];
        for (var it_2 = this.gen(), next = it_2.next(); !next.done; next = it_2.next()) {
            result.push(next.value);
        }
        return result;
    };
    Pyramider.prototype.gen = function () {
        var i;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    i = 1;
                    _a.label = 1;
                case 1:
                    if (!(i < this.levels)) return [3 /*break*/, 4];
                    return [4 /*yield*/, [Math.floor(this.width / Math.pow(2, i)), Math.floor(this.height / Math.pow(2, i))]];
                case 2:
                    _a.sent();
                    _a.label = 3;
                case 3:
                    ++i;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/];
            }
        });
    };
    return Pyramider;
}());
exports.Pyramider = Pyramider;
// export class TilePyramider {
//     constructor(
//         private tiler: LevelTiler,
//         private pyramider: Pyramider,
//     ) {}
// }
var TileCommand = /** @class */ (function () {
    function TileCommand(tile, inputName, outputName, preinput, preoutput) {
        if (inputName === void 0) { inputName = 'input.mp4'; }
        if (outputName === void 0) { outputName = 'output.mp4'; }
        if (preinput === void 0) { preinput = ''; }
        if (preoutput === void 0) { preoutput = ''; }
        this.tile = tile;
        this.inputName = inputName;
        this.outputName = outputName;
        this.preinput = preinput;
        this.preoutput = preoutput;
    }
    TileCommand.prototype.get = function () {
        var _a = this, tile = _a.tile, preinput = _a.preinput, preoutput = _a.preoutput, inputName = _a.inputName, outputName = _a.outputName;
        // console.log(tile)
        return "ffmpeg " + preinput + " -i " + inputName + " -vf " +
            ("'crop=w=" + tile[1] + ":h=" + tile[2] + ":x=" + tile[3] + ":y=" + tile[4] + "' ") +
            (preoutput + " " + outputName);
    };
    return TileCommand;
}());
exports.TileCommand = TileCommand;
var PyramidCommand = /** @class */ (function () {
    function PyramidCommand(width, height, input, zoom, preinput, preoutput) {
        if (input === void 0) { input = '0'.padStart(6, '0') + '.mp4'; }
        if (zoom === void 0) { zoom = 0; }
        if (preinput === void 0) { preinput = ''; }
        if (preoutput === void 0) { preoutput = ''; }
        this.width = width;
        this.height = height;
        this.input = input;
        this.zoom = zoom;
        this.preinput = preinput;
        this.preoutput = preoutput;
    }
    PyramidCommand.prototype.get = function () {
        var _a = this, width = _a.width, height = _a.height, input = _a.input, preinput = _a.preinput, preoutput = _a.preoutput, zoom = _a.zoom;
        var zoomPadded = ('' + zoom).padStart(6, '0');
        return "ffmpeg " + preinput + " -i " + input + " -vf 'scale=" + width + ":" + height + "' " + preoutput + " " + zoomPadded + ".mp4";
    };
    return PyramidCommand;
}());
exports.PyramidCommand = PyramidCommand;
