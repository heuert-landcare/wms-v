import { Pyramider, LevelTiler, TileCommand, PyramidCommand } from "./tiler-pyramider";
import _ from 'underscore'

export function getArray(generator: any) {
    const result = []
    for (
        let gen = generator(), next = gen.next();
        !next.done;
        next = gen.next()
    ) {
        result.push(next.value)
    }
    return result
}

export interface FilterAndFilenames {
    filter: string
    sourceFilename: string
    targetFilename: string
}
export class FfmpegTool {
    private pyramider: Pyramider
    private tiler: LevelTiler
    constructor(
        private width: number,
        private height: number,
        private levels: number,
        private sourceFilename: string = 'source.ext',
        private tileWidth: number = 256,
        private tileHeight: number = 256,
    ) {}
    tile(pyramid: number[][]) {
        const tiles = pyramid.map((p, i) => {
            return new LevelTiler(p[0], p[1], pyramid.length - i - 1, this.tileWidth, this.tileHeight).get()
        })
        const result = tiles.map(ts => {
            return ts.map((t: number[]) => {
                let zoom = t[0]
                let paddedZoom = ('' + zoom).padStart(6, '0')
                return new TileCommand(t, `${paddedZoom}.mp4`, this.getFilename(zoom, t[5], t[6])).get()
            })
        })
        return result.slice(0, -1).reverse()
    }
    commandGenerator() {
        const pyramider = new Pyramider(this.width, this.height, this.levels)
        let pyramid = pyramider.get()
        let result = pyramid.map((p, i) => {
            const zoom = pyramid.length - i
            const zoomPadded = ('' + zoom).padStart(6, '0')
            return new PyramidCommand(p[0], p[1], `${zoomPadded}.mp4`, zoom - 1).get()
        }).concat(_.flatten(this.tile([[this.width, this.height]].concat(pyramid))))
        return _.flatten(result)
    }
    getFilename(zoom: number, tileX: number, tileY: number, extension: string = 'mp4', padLength: number = 6): string {
        const z = `${zoom}`.padStart(padLength, '0')
        const x = `${tileX}`.padStart(padLength, '0')
        const y = `${tileY}`.padStart(padLength, '0')
        return `${z}/${y}/${x}.${extension}`
    }
}
