"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
// spawn("ls").stdout.pipe(process.stdout)
const days = 365;
/*
const cpus = 8
const percpu = days / cpus
const partitions = []
for (let i = 1; i <= cpus; ++i) {
    partitions.push(Math.floor(percpu * i))
}
console.log(partitions)
async function processLayer() {
    await
}
for (const p of partitions) {
    const promise = new Promise(resolve => {

    })
    let year = _.padStart(`${p}`, 3, "0")
    let command = `raster2pgsql -b 1 -Y -l 2,4,8,16,32,64,128,256 -F -t 100x100 -I -R lst_average_2015_${year}.kea data.r_lst_average_2015_${year} | psql -d wmsv`
}

process.exit(0)
for (let i = 1; i <= percpu; ++i) {
    let year = _.padStart(`${i}`, 3, "0")
    let command = `raster2pgsql -b 1 -Y -l 2,4,8,16,32,64,128,256 -F -t 100x100 -I -R lst_average_2015_${year}.kea data.r_lst_average_2015_${year} | psql -d wmsv`
}
*/
for (let i = 1; i <= days; ++i) {
    // days of year
    // raster2pgsql -b 1 -Y -l 2 -F -t 100x100 -I -R lst_average_2015_001.kea data.r_lst_average_2015_001 | psql -d wmsv
    // days.push(i)
    let year = _.padStart(`${i}`, 3, "0");
    // let command = `PGPASSWORD=wmsv raster2pgsql -s 3031 -d -b 1 -Y -l 2,4,8,16,32,64,128,256 -F -t 100x100 -I -R /space/lst_average/lst_average_2015_${year}.kea data.r_lst_average_2015_${year} | psql -U wmsv -d wmsv`
    let command = `raster2pgsql -s 3031 -d -b 1 -Y -l 2,4,8,16,32,64,128,256 -F -t 100x100 -I -R /space/lst_average/lst_average_2015_${year}.kea data.r_lst_average_2015_${year} | psql -d wmsv`;
    // console.log("executing", command)
    console.log(command);
    /*
    let c = exec(command)
    c.stdout.pipe(process.stdout)
    c.stderr.pipe(process.stderr)
    */
}
//# sourceMappingURL=index.js.map