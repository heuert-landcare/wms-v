// declare function multistream(streams: Stream[]): Stream
// declare module 'multistream': (streams: Stream[]) => Stream;
// export as namespace multistream;


/*
declare module 'multistream' {
	import { Stream } from 'stream';
	// const multistream: (streams: Stream[]) => Stream;
	function multistream(streams: Stream[]): Stream;
	export default multistream;
}
 */

/*
declare module 'multistream' {
	// import { Stream } from 'stream';
	// export default function multistream(a:any):any;
	function MultiStream(a:any):any;
	export default MultiStream;
}
 */
declare module 'saxpath' {
	// function SaXPath(a: any, xpath: string): any
	class SaXPath {
		constructor(stream: any, xpath: string)
		on(event: string, func: (xml: any) => void): void
	}
}
