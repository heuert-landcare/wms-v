import * as expect from "expect"
import { TimeInterval } from "../src/util/intervals"

describe("interval parsing", () => {
	it("can create 1 intervals at 1 year", () => {
		const expected = [
			"2018-01-01T00:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-01T00:00:00.0Z/2018-01-01T00:00:00.0Z/P1Y").get()
		expect(real).toEqual(expected)
	})
	it("can create 3 intervals at 2 days", () => {
		const expected = [
			"2018-01-01T00:00:00.000Z",
			"2018-01-03T00:00:00.000Z",
			"2018-01-05T00:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-01T00:00:00.0Z/2018-01-05T00:00:00.000Z/P2D").get()
		expect(real).toEqual(expected)
	})
	it("can create 3 intervals at 2 hours", () => {
		const expected = [
			"2018-01-01T00:00:00.000Z",
			"2018-01-01T02:00:00.000Z",
			"2018-01-01T04:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-01T00:00:00.0Z/2018-01-01T04:00:00.000Z/P2h").get()
		expect(real).toEqual(expected)
	})
	it("can create 3 intervals at 3 weeks", () => {
		const expected = [
			"2018-01-01T00:00:00.000Z",
			"2018-01-22T00:00:00.000Z",
			"2018-02-12T00:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-01T00:00:00.0Z/2018-02-18T00:00:00.000Z/P3W").get()
		expect(real).toEqual(expected)
	})
	it("can create 4 intervals at 1 month", () => {
		const expected = [
			"2018-01-09T00:00:00.000Z",
			"2018-02-09T00:00:00.000Z",
			"2018-03-09T00:00:00.000Z",
			"2018-04-09T00:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-09T00:00:00.0Z/2018-04-09T00:00:00.000Z/P1M").get()
		expect(real).toEqual(expected)
	})
	it("can create 4 intervals at 1 year", () => {
		const expected = [
			"2018-01-09T00:00:00.000Z",
			"2019-01-09T00:00:00.000Z",
			"2020-01-09T00:00:00.000Z",
			"2021-01-09T00:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-09T00:00:00.0Z/2021-01-09T00:00:00.000Z/P1Y").get()
		expect(real).toEqual(expected)
	})
	it("can create 4 intervals at 3 months", () => {
		const expected = [
			"2012-01-31T00:00:00.000Z",
			"2012-04-30T00:00:00.000Z",
			"2012-07-31T00:00:00.000Z",
			"2012-10-31T00:00:00.000Z",
		]
		const real = new TimeInterval("2012-01-31T00:00:00.000Z/2012-10-31T00:00:00.000Z/P3M").get()
		expect(real).toEqual(expected)
	})
	it("can create 4 intervals at 1 quarter", () => {
		const expected = [
			"2018-01-09T00:00:00.000Z",
			"2018-04-09T00:00:00.000Z",
			"2018-07-09T00:00:00.000Z",
			"2018-10-09T00:00:00.000Z",
		]
		const real = new TimeInterval("2018-01-09T00:00:00.0Z/2018-10-09T00:00:00.000Z/P1Q").get()
		expect(real).toEqual(expected)
	})
	it("can create 4 intervals from 2 different types", () => {
		const expected = [
			"2018-01-09T00:00:00.000Z",
			"2018-04-09T00:00:00.000Z",
			"2018-07-09T00:00:00.000Z",
			"2018-07-10T00:00:00.000Z",
		]
		const real = TimeInterval.getIntervalsFromQueryString("2018-01-09T00:00:00Z/2018-04-09T00:00:00.000Z/P1Q,2018-07-09T00:00:00Z/2018-07-10T00:00:00.000Z/P1D")
		expect(real).toEqual(expected)
	})
})