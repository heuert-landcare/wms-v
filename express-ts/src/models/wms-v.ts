import { WebService } from "./webservice"
import { Request, Response } from "express"
// import async = require("async")
// import request = require("request")
// import * as process from "child_process"
import * as fs from "fs"
// import { DOMParser } from "xmldom"
// import * as xpath from "xpath"
import * as _ from "lodash"
// import * as https from "https"
// import * as util from "util"
import { exec, spawn, ChildProcess } from "child_process"
// import multistream = require("multistream")
// import { app } from "../app"
import { TimeInterval } from "../util/intervals";
import { stringify } from "querystring"
import { priorityQueue } from "async"
import { WmsTime } from "./wms-time";
import { EventEmitter } from "events";
import { Readable } from "stream";

const ffmpegCommand = fs.existsSync(__dirname + "/../../../ffmpeg-4.0.1") && fs.realpathSync(__dirname + "/../../../ffmpeg-4.0.1/ffmpeg")

class CommandLineTask {
	private promise: Promise<ChildProcess>
	private cp: ChildProcess
	private resolver: (cp: ChildProcess) => void
	constructor(
		private command: string,
		private args: string[],
	) {
		this.promise = new Promise<ChildProcess>((resolve, reject) => {
			this.resolver = resolve
		})
	}
	exec(): ChildProcess {
		this.cp = spawn(this.command, this.args)
		this.resolver(this.cp)
		return this.cp
	}
	getPromise(): Promise<ChildProcess> {
		return this.promise
	}
}

export class WmsV extends WebService {
	static cacheFolder = "/tmp/videoCache"
	static markedAsEncoding: any = {}
	static queue = priorityQueue((task: CommandLineTask, callback: () => void) => {
		const cp = task.exec()
		cp.stdout.once("end", () => {
			console.log(`calling callback queue worker`)
			callback()
		})
	}, 1)
	static eventBus: EventEmitter
	cacheFilename: string
	constructor(req: Request, res: Response) {
		super(req, res)
		WmsV.queue.drain = () => {
			console.log(`queue drained`)
		}
		WmsV.eventBus = new EventEmitter
	}
	public noneMatch(): boolean {
		return !fs.existsSync(this.cacheFilename)
	}
	loopReader(): void {
		/*
		let reader
		let haveRead = 0
		const looper = () => {
			const size = fs.statSync(this.cacheFilename).size
			if (size < haveRead) {
				setTimeout(looper, 2000)
				return
			}
			reader = fs.createReadStream(this.cacheFilename, {
				start: haveRead
			})
			reader.on("data", (data) => {
				haveRead += data.length
				res.write(data)
			})
			reader.on("end", () => {
				if (WmsV.markedAsEncoding[this.cacheFilename]) {
					looper()
				} else {
					res.end()
				}
			})
		}
		looper()
		*/
		if (WmsV.markedAsEncoding[this.cacheFilename]) {
			setTimeout(() => this.loopReader(), 500)
			return
		}
		const res = this.res
		const reader = fs.createReadStream(this.cacheFilename)
		reader.pipe(res)
	}
	serveCached(hashCache: string): void {
		console.log(`Serving`, this.cacheFilename)
		const req = this.req
		const res = this.res
		if (false && !WmsV.markedAsEncoding[this.cacheFilename] && !this.noneMatch()) {
			res.writeHead(304, {
				"Access-Control-Allow-Origin": "*",
				"Accept-Ranges": "bytes",
				"Content-Type": "video/mp4",
				"ETag": hashCache
			});
			return res.end()
		}
		if (WmsV.markedAsEncoding[this.cacheFilename]) {
			this.checkFileZeroSizeAndAfter(this.cacheFilename, () => {
				this.loopReader()
			})
			return
		}
		const total = fs.statSync(this.cacheFilename).size
		if (req.headers.range) {
			const range = req.headers.range
			const replaced = typeof range === "string" ? range.replace(/bytes=/, "") : range[0].replace(/bytes=/, "")
			const parts = replaced.split("-")
			const partialstart = parts[0];
			const partialend = parts[1] || "";

			const start = parseInt(partialstart, 10);
			const end = partialend ? parseInt(partialend, 10) : total - 1;
			const chunksize = (end - start) + 1;
			let readStream
			if (end < start) {
				readStream = fs.createReadStream(this.cacheFilename, {
					start
				});
			} else {
				readStream = fs.createReadStream(this.cacheFilename, {
					start,
					end
				});
			}
			res.writeHead(206, {
				"Access-Control-Allow-Origin": "*",
				"Content-Range": "bytes " + start + "-" + end + "/" + total,
				"Accept-Ranges": "bytes",
				"Content-Length": chunksize,
				"Content-Type": "video/mp4",
				"ETag": hashCache
			});
			readStream.pipe(res);
		} else {
			this.res.writeHead(200, {
				"Access-Control-Allow-Origin": "*",
				"Connection": "Keep-Alive",
				"Content-Type": "video/mp4",
				"Content-Length": total,
				"ETag": hashCache
			})
			fs.createReadStream(this.cacheFilename).pipe(this.res)
		}
	}
	checkFileZeroSizeAndAfter(fileName: string, func: () => void): void {
		fs.stat(fileName, (err, stat) => {
			if (stat.size === 0) {
				setTimeout(() =>
					this.checkFileZeroSizeAndAfter(fileName, func), 200)
				return
			}
			if (!err) {
				func()
			} else {
				console.log(`error (checkFileZeroSizeAndAfter): ${err}`)
			}
		})
	}
	get(): void {
		const hashCache = this.getParamHashed()
		this.cacheFilename = `${WmsV.cacheFolder}/${hashCache}.mp4`
		// use cached if available and send in chunks
		if (fs.existsSync(this.cacheFilename)) {
			this.serveCached(hashCache)
		} else {
			this.res.writeHead(200, {
				"Access-Control-Allow-Origin": "*",
				"Connection": "Keep-Alive",
				"Content-Type": "video/mp4"
			})
			this.streamVideo()
		}
	}
	public static createCacheFolderIfNotExists(): void {
		if (!fs.existsSync(WmsV.cacheFolder)) {
			fs.mkdirSync(WmsV.cacheFolder)
		}
	}
	getImageCachedFileName(params: string): string {
		const hashCache = WebService.hashParamsString(params)
		return `${WmsTime.cacheFolder}/${hashCache}.png`
	}
	streamVideo(): void {
		const params = this.params
		const res = this.res
		const interval = TimeInterval.getIntervalsFromQueryString(params.interval)
		// const filteredImages = images.filter((image: any) => isInTime(image, interval))
		const fps = Number(params.fps || 25)
		// const args = `-y -f image2pipe -vcodec png -r ${fps} -i - -vcodec libvpx -auto-alt-ref 0 -f webm -`
		// const imagesInFolder = filteredImages.map(i => `frames/${i}`)
		// const command = `cat ${imagesInFolder.join(" ")} | /Users/tim/ffmpeg ${args}`
		// const cArgs = command.split(" ")
		// http://localhost:3000/wms?time=2009-01-01T00:00:00.000Z&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A2193&BBOX=1722488,5241866,2083732,5603110&WIDTH=512&HEIGHT=512&map=/opt/mapserver/mapfiles/possum_density.map
		// const ws = fs.createWriteStream("/tmp/ff.bash")
		const streams = interval.map(i => `${this.params.url}?${stringify(params)}&time=${i}`)
		// const cachedStream = fs.createWriteStream("/tmp/cached.log", "utf8")
		const cachedImages = interval.map(i => {
			params.time = i
			const ps = stringify(params)
			const s = WmsTime.getCacheFile(ps)
			// cachedStream.write(`${ps}`)
			// cachedStream.write(`cached: ${s}\n`)
			return s
		})
		// cachedStream.close()
		/**
		 * Try
		 * http://localhost:3000/wms-v?service=WMS-V&request=GetMap&interval=2000-02-18/2000-02-26/P8D,2000-03-06/2000-12-27/P8D,2001-01-01/2001-06-10/P8D,2001-06-26/2001-12-27/P8D&fps=3&url=https%3A%2F%2Fneo.sci.gsfc.nasa.gov%2Fwms%2Fwms&layers=MOD11C1_E_LSTDA&crs=CRS:84&bbox=0,10,60,50&width=1920&height=1080
		 */
		// Works! http://localhost:3000/wms-v?interval=2000-02-18/2000-02-26/P8D,2000-03-06/2000-12-27/P8D&fps=1&version=1.3.0&service=WMS&request=GetMap&layers=MOD11C1_E_LSTDA&crs=CRS:84&bbox=0,10,60,50&width=1920&height=1080&format=image/png&url=https%3A%2F%2Fneo.sci.gsfc.nasa.gov%2Fwms%2Fwms

		const firstArgs = [
			"-probesize", "2147483647",
			"-analyzeduration", "2147483647",
		]
		const ft = params.vfilter_time || 1
		const fileArgs = streams.map((s, i) => {
			const cachedImage = cachedImages[i]
			// console.log(`trying ${cachedImage}`)
			let imageUri = s
			if (fs.existsSync(cachedImage)) {
				// console.log(`exists`)
				imageUri = cachedImage
			}
			return [
			`-thread_queue_size`, `128`,
			`-framerate`, `${fps}`, "-loop", `1`, `-t`, `${ft}`, "-i",
			`${imageUri}`]
		}).reduce((a, f) => a.concat(f), [])
		const ffArgs = firstArgs.concat(fileArgs)
		const afterArgs = [
			"-vcodec", "libx264",
			"-f", "mp4",
			"-movflags", "frag_keyframe+empty_moov+faststart",
			"-frag_duration", "3600",
			"pipe:1"
		]
		// console.log(`path = ${ffmpegCommand}`)
		let filter = ""
		let concat = ""
		let blend = false
		if (params.vfilter && params.vfilter === "blend") {
			blend = true
		}
		let len = streams.length - 1
		for (let i = 0; i < len; ++i) {
			if (blend) {
				filter += `[${i + 1}:v][${i}:v]blend=all_expr='` +
				`A*(if(gte(T,${ft}),1,T/${ft}))+` +
				`B*(1-(if(gte(T,${ft}),1,T/${ft})))'[v${i}]; `
			}
			concat += `[v${i}]`
		}
		if (!blend) {
			concat = ""
			len = streams.length
			for (let i = 0; i < len; ++i) {
				concat += `[${i}:v]`
			}
		}
		filter += `${concat}concat=n=${len}:v=1:a=0[v]; `
		filter += `[v]split=2[bg][fg];[bg]drawbox=c=#00FF00@1:replace=1:t=fill[bg]; `
		filter += `[bg][fg]overlay=format=auto[vv]`
		/*
		` -filter_complex \
		"[1:v][0:v]blend=all_expr='A*(if(gte(T,3),1,T/3))+B*(1-(if(gte(T,3),1,T/3)))'[v0]; \
		[2:v][1:v]blend=all_expr='A*(if(gte(T,3),1,T/3))+B*(1-(if(gte(T,3),1,T/3)))'[v1]; \
		[3:v][2:v]blend=all_expr='A*(if(gte(T,3),1,T/3))+B*(1-(if(gte(T,3),1,T/3)))'[v2]; \
		[4:v][3:v]blend=all_expr='A*(if(gte(T,3),1,T/3))+B*(1-(if(gte(T,3),1,T/3)))'[v3]; \
		[v0][v1][v2][v3]concat=n=4:v=1:a=0[v]" -map "[v]" `
		*/
		/*
		const command = `${ffmpegCommand} ` + ffArgs.join(" ") +
			(filter.length > 0 ? ` ${filter} ` : "") + afterArgs.join(" ")
			*/
		// ws.write(command)
		// ws.end()
		const filterArray = filter.length > 0 ? ["-filter_complex", filter, "-map", "[vv]"] : []
		const cargs = ffArgs.concat(filterArray).concat(afterArgs)
		const command = new CommandLineTask(ffmpegCommand, cargs)
		console.log("queue length", WmsV.queue.length())
			/*
		command.getPromise().then((cb) => {
			cb.stdout.once("data", () => {
				// after all bytes are written to cache file
				fs.createReadStream(this.cacheFilename).pipe(res)
			})
		})*/
		WmsV.queue.push(command, 1, (err) => {
			console.log(`command end ERROR:`, err)
		})
		/*
		const ffmpeg = spawn(ffmpegCommand, cargs);
		ffmpeg.stdout.once("end", () => {
			// after first bytes are written to cache file
			fs.createReadStream(this.cacheFilename).pipe(res)
			// res.write(data)
		})
		*/
		command.getPromise().then(cp => {
			this.markEncoding()
			const cachefileStream = fs.createWriteStream(this.cacheFilename)
			cp.stdout.pipe(res)
			cp.stdout.pipe(cachefileStream)
			cp.stderr.pipe(process.stderr)
			cp.stdout.once("end", () => {
				delete WmsV.markedAsEncoding[this.cacheFilename]
				WmsV.eventBus.emit("encoding-finished", this.cacheFilename)
			})
		})
		// command..stdout.pipe(cachefileStream)
		// ffmpeg.stderr.pipe(process.stderr)
		// ffmpeg.stdout.pipe(res)
		// const ws = fs.createWriteStream("/tmp/ffm.js", "utf8")
		// ws.write(`const c = '${ffmpegCommand}'; \nconst args = ['${cargs.map(a => a.replace("'", "\\'")).join("', '")}']`)
		// ws.end()
		// ws.close()
		/*
		// console.log(`${ffmpegCommand} ` + ffArgs.join(" "))
		// ffmpeg.stdout.pipe(res)
		ffmpeg.stdout.on("end", () => {
			cachefileStream.end()
			res.end()
		})
		// const rs = fs.createReadStream(this.cacheFilename)
		// rs.pipe(res)
		*/
	}
	markEncoding(): void {
		WmsV.markedAsEncoding[this.cacheFilename] = true;
	}
}
WmsV.createCacheFolderIfNotExists()