
export class Composite {
	constructor() { }
	test() {
		return "hello composite"
	}
	get() {
		// return `ffmpeg -i movies_2x/20181009062000/02/003.mp4 -i movies_2x/20181009062000/02/004.mp4 -filter_complex '[0:v]pad=iw*2:ih[int];[int][1:v]overlay=W/2:0[vid]' -map '[vid]' -c:v libx264 out.mp4`
		return `ffmpeg -i movies_2x/20181009062000/02/003.mp4 -i movies_2x/20181009062000/02/004.mp4 -i movies_2x/20181009062000/02/006.mp4 -i movies_2x/20181009062000/02/007.mp4  -filter_complex '[0:v]pad=iw*2:ih*2[i0];[i0][1:v]overlay=W/2:0[i1];[i1][2:v]overlay=0:H/2[i2];[i2][3:v]overlay=W/2:H/2[vid]' -map '[vid]' -c:v libx264 out2.mp4`
	}
}


const c = new Composite()
c.get() // ?