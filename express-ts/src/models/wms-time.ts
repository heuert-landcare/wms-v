import { WebService } from "./webservice";
import { Request, Response } from "express";
import request = require("request")
import { stringify } from "query-string"
import * as _ from "lodash"
import * as fs from "fs"


export class WmsTime extends WebService {
	static cacheFolder = "/tmp/wmstCache"
	cacheFilename: string
	get(): void {
		const req = this.req
		const res = this.res
		// https://npm.landcareresearch.co.nz/cgi-bin/mapserv?LAYERS=possum_2010&
		// TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&
		// STYLES=&SRS=EPSG%3A2193&BBOX=1722488,5241866,2083732,5603110&WIDTH=512&HEIGHT=512&
		// map=/opt/mapserver/mapfiles/possum_density.map
		const time = this.params.time
		const year = (new Date(time)).getFullYear()
		const params = this.params
		const url = "https://npm.landcareresearch.co.nz/cgi-bin/mapserv" // params.url
		params.layers = `possum_${year}`
		// '2009-01-01T00:00:00.000Z,2010-01-01T00:00:00.000Z,2011-01-01T00:00:00.000Z,2012-01-01T00:00:00.000Z,2013-01-01T00:00:00.000Z,2014-01-01T00:00:00.000Z,2015-01-01T00:00:00.000Z,2016-01-01T00:00:00.000Z,2017-01-01T00:00:00.000Z,2018-01-01T00:00:00.000Z,2019-01-01T00:00:00.000Z,2020-01-01T00:00:00.000Z,2021-01-01T00:00:00.000Z,2022-01-01T00:00:00.000Z,2023-01-01T00:00:00.000Z,2024-01-01T00:00:00.000Z,2025-01-01T00:00:00.000Z,2026-01-01T00:00:00.000Z,2027-01-01T00:00:00.000Z,2028-01-01T00:00:00.000Z,2029-01-01T00:00:00.000Z,2030-01-01T00:00:00.000Z,2031-01-01T00:00:00.000Z,2032-01-01T00:00:00.000Z,2033-01-01T00:00:00.000Z,2034-01-01T00:00:00.000Z,2035-01-01T00:00:00.000Z,2036-01-01T00:00:00.000Z,2037-01-01T00:00:00.000Z,2038-01-01T00:00:00.000Z
		// http://localhost:3000/wms?time=2038-01-01T00:00:00.000Z&LAYERS=possum_2010&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A2193&BBOX=1722488,5241866,2083732,5603110&WIDTH=512&HEIGHT=512&map=/opt/mapserver/mapfiles/possum_density.map&url=https%3A%2F%2Fnpm.landcareresearch.co.nz%2Fcgi-bin%2Fmapserv

		// Requests like:
		// http://localhost:3000/wms?time=2009-01-01T00:00:00.000Z&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A2193&BBOX=1722488,5241866,2083732,5603110&WIDTH=512&HEIGHT=512&map=/opt/mapserver/mapfiles/possum_density.map&url=https%3A%2F%2Fnpm.landcareresearch.co.nz%2Fcgi-bin%2Fmapserv
		const myUrl = `${url}?` + stringify(params)
		const r = request.get(myUrl)
		r.pipe(res)
	}
	public static getCacheFile(params: string): string {
		const hashCache = WebService.hashParamsString(params)
		const cacheFilename = `${WmsTime.cacheFolder}/${hashCache}.png`
		return cacheFilename
	}
	serveFromCache(): boolean {
		const params = stringify(this.params)
		const hashCache = WebService.hashParamsString(params)
		const hours24 = 24 * 3600
		this.res.writeHead(206, {
			"Cache-Control": `max-age=${365 * hours24}`,
			"ETag": hashCache
		})
		this.cacheFilename = WmsTime.getCacheFile(params)
		// const cacheStream = fs.createWriteStream("/tmp/cacheWmsTime.log", "utf8")
		if (fs.existsSync(this.cacheFilename)) {
			// cacheStream.write(`serving`, this.cacheFilename)
			// cacheStream.write(`params`, params)
			fs.createReadStream(this.cacheFilename).pipe(this.res)
			// cacheStream.close()
			return true
		}
		return false
	}
	proxy(): void {
		const req = this.req
		const res = this.res
		// req.url = req.originalUrl    //<-- Just this!
		const params = _.mapKeys(req.query, (v, k: string) =>
			k.toLowerCase()
		)
		if (this.serveFromCache()) {
			return
		}
		const cacheStream = fs.createWriteStream(this.cacheFilename)
		const time: string = <string> params.time
		const oneDay = 1000 * 60 * 60 * 24
		const dateTime = new Date(time)
		const start = new Date(dateTime.getFullYear(), 0, 0)
		const diff = dateTime.getTime() - start.getTime()
		const day = Math.floor(diff / oneDay)
		const url = "http://localhost:8080/cgi-bin/mapserv" // params.url
		params.layers = `${params.layers}_` + `${day}`.padStart(3, "0")
		// '2009-01-01T00:00:00.000Z,2010-01-01T00:00:00.000Z,2011-01-01T00:00:00.000Z,2012-01-01T00:00:00.000Z,2013-01-01T00:00:00.000Z,2014-01-01T00:00:00.000Z,2015-01-01T00:00:00.000Z,2016-01-01T00:00:00.000Z,2017-01-01T00:00:00.000Z,2018-01-01T00:00:00.000Z,2019-01-01T00:00:00.000Z,2020-01-01T00:00:00.000Z,2021-01-01T00:00:00.000Z,2022-01-01T00:00:00.000Z,2023-01-01T00:00:00.000Z,2024-01-01T00:00:00.000Z,2025-01-01T00:00:00.000Z,2026-01-01T00:00:00.000Z,2027-01-01T00:00:00.000Z,2028-01-01T00:00:00.000Z,2029-01-01T00:00:00.000Z,2030-01-01T00:00:00.000Z,2031-01-01T00:00:00.000Z,2032-01-01T00:00:00.000Z,2033-01-01T00:00:00.000Z,2034-01-01T00:00:00.000Z,2035-01-01T00:00:00.000Z,2036-01-01T00:00:00.000Z,2037-01-01T00:00:00.000Z,2038-01-01T00:00:00.000Z
		// http://localhost:3000/wms?time=2038-01-01T00:00:00.000Z&LAYERS=possum_2010&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A2193&BBOX=1722488,5241866,2083732,5603110&WIDTH=512&HEIGHT=512&map=/opt/mapserver/mapfiles/possum_density.map&url=https%3A%2F%2Fnpm.landcareresearch.co.nz%2Fcgi-bin%2Fmapserv

		// Requests like:
		// http://localhost:3000/wms?time=2009-01-01T00:00:00.000Z&TRANSPARENT=true&FORMAT=image%2Fpng&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&SRS=EPSG%3A2193&BBOX=1722488,5241866,2083732,5603110&WIDTH=512&HEIGHT=512&map=/opt/mapserver/mapfiles/possum_density.map&url=https%3A%2F%2Fnpm.landcareresearch.co.nz%2Fcgi-bin%2Fmapserv
		const myUrl = `${url}?` + stringify(params)
		// res.end(params.layers)
		console.log(`proxying`, myUrl)
		const r = request.get(myUrl)
		r.pipe(res)
		r.pipe(cacheStream)
	}
}