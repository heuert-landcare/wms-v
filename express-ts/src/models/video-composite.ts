import { WebService } from "./webservice"
import { Request, Response } from "express"
import { exec } from "child_process"

function round2(n: number): number {
	const rounded = Math.round(n)
	return rounded % 2 === 0 ? rounded : rounded + 1
}
export class VideoComposite extends WebService {
	constructor(req: Request, res: Response) {
		super(req, res)
	}
	get() {
		const file = "/space/data/himawari/resize_movies/2750x2750.mp4" // this.params.file
		const fileWidth = 2750
		const fileHeight = 2750
		const cropper = new Cropper(file, fileWidth, fileHeight)
		const width = this.params.width
		const height = this.params.height
		this.res.writeHead(200, {
			"Access-Control-Allow-Origin": "*",
			"Connection": "Keep-Alive",
			"Content-Type": "video/mp4",
			// "transfer-encoding": ""
		})
		// this.res.end(cropper.cropBbox(this.params.bbox, width, height))
		const command = "/usr/bin/" + cropper.cropBbox(this.params.bbox, width, height)
		console.log(command)
		const x = exec(command, { encoding: "buffer", maxBuffer: 0.5 * Math.pow(1024, 3) })
		x.stdout.pipe(this.res, { end: true })
		// x.stdout.on("data", data => this.res.write(data))
		// x.stdout.once("end", () => this.res.end())
		x.stderr.pipe(process.stderr)
		// x.stdout.once("close", () => this.res.end())
		this.req.once("close", () => {
			x.kill()
		})
	}
}

export class Cropper {
	protected width: number
	protected height: number
	constructor(
		protected inputFile: string,
		width: number,
		height: number
	) {
		this.width = round2(width)
		this.height = round2(height)
	}
	crop(x: number, y: number, width: number, height: number): string {
		const command = `ffmpeg -i ${this.inputFile} -filter_complex ` +
			`'[0:v]${this.getCropFilter(x, y, width, height)}[vid]' -map '[vid]' ` +
			this.getOutputParameters()
		return command
	}
	beforeArgs(): string {
		return "-probesize 2147483647 -analyzeduration 2147483647"
	}
	cropBbox(bbox: string, width: number, height: number): string {
		width = round2(width)
		height = round2(height)
		const command = `ffmpeg ${this.beforeArgs()} -i ${this.inputFile} -filter_complex ` +
			`'[0:v]${this.getCropScaleFilter(bbox, width, height)}[vid]' -map '[vid]' ` +
			this.getOutputParameters()
		return command
	}
	getCropFilter(x: number, y: number, width: number, height: number): string {
		y = this.height - y
		const pad = this.getPadFilter(x, y, width, height)
		if (x < 0 && y < 0) {
			x = 0
			y = 0
		} else if (x < 0) {
			x = 0
		} else if (y < 0) {
			y = 0
		}
		const crop = `crop=${width}:${height}:${x}:${y}`
		const padCrop = pad ? `${pad}[pad];[pad]${crop}` : crop
		return padCrop
	}
	getPadFilter(x: number, y: number, width: number, height: number): string {
		// "-500,-500,500,1500", 1000, 1000
		if (x < 0 || y < 0) {
			if (x < 0 && y < 0) {
				const padX = round2(-x + width)
				const padY = round2(-y + height)
				return `pad=${padX}:${padY}:${-x}:${-y}`
			}
			if (x < 0) {
				const padX = round2(-x + width)
				return `pad=${padX}:${height}:${-x}`
			}
			if (y < 0) {
				const padY = round2(-y + height)
				return `pad=${width}:${padY}:0:${-y}`
			}
		}
		return ""
	}
	getScaleFilter(width: number, height: number): string {
		return `scale=${width}:${height}`
	}
	getScaleCropFilter(bbox: string, width: number, height: number): string {
		const bb = new BboxParser(bbox, this.width, this.height)
		const scaledX = this.scale(bb.getX(), bb.getWidth(), width)
		const scaledY = this.scale(bb.getY(), bb.getHeight(), height)
		const scaledWidth = this.scale(bb.getWidth(), bb.getWidth(), width)
		const scaledHeight = this.scale(bb.getHeight(), bb.getHeight(), height)
		return this.getScaleFilter(width, height) + "[scaled];[scaled]" +
			this.getCropFilter(scaledX, scaledY, scaledWidth, scaledHeight)
	}
	getCropScaleFilter(bbox: string, width: number, height: number): string {
		const bb = new BboxParser(bbox, this.width, this.height)
		const bbWidth = bb.getWidth()
		const bbHeight = bb.getHeight()
		return this.getCropFilter(bb.getX(), bb.getY(), bbWidth, bbHeight) +
			"[cropped];[cropped]" +
			this.getScaleFilter(width, height)
	}
	scale(input: number, from: number, to: number): number {
		// 200 100 => 0.5
		return round2(input * (from / to))
	}
	getOutputParameters(): string {
		const flags = "-movflags frag_keyframe+empty_moov+faststart -frag_duration 3600"
		return `-f mp4 ${flags} pipe:1`
	}
}

export class BboxParser {
	constructor(protected bbox: string, protected width: number, protected height: number) { }
	getX(): number {
		return this.getPartsNumbers()[0] + this.width / 2
	}
	getY(): number {
		return this.getPartsNumbers()[3] + this.height / 2
	}
	getWidth(): number {
		const parts = this.getPartsNumbers()
		return parts[2] - parts[0]
	}
	getHeight(): number {
		const parts = this.getPartsNumbers()
		return parts[3] - parts[1]
	}
	getParts(): string[] {
		return this.bbox.split(",")
	}
	getPartsNumbers(): number[] {
		return this.getParts().map(Number)
	}
	getUpperLeft(): number[] {
		const parts = this.getPartsNumbers()
		return [
			parts[0],
			parts[3]
		]
	}
}

function test() {
	const cropper = new Cropper("myfile.mp4", 2000, 2000)
	const c = cropper.cropBbox("-500,-500,500,1500", 1000, 1000)
	c.substring(85, 160) // ?
	// const cropper1 = new Cropper("some.mp4", 2750, 2750)
	// const c1 = cropper1.cropBbox("-1375,-1475,1375,1375", 1700, 1700)
	// c1.substring(85, 160) // ?
}
test()