import { Response, Request, NextFunction } from "express";
import * as _ from "lodash";
import { createHash } from "crypto"
import { stringify } from "query-string"

export class WebService {
	protected questionMarkIndex: number
	protected port: string
	protected url: string
	protected baseUrl: string
	protected params: any
	constructor(protected req: Request, protected res: Response) {
		this.questionMarkIndex = req.url.indexOf("?")
		this.baseUrl = req.url.substring(0, this.questionMarkIndex === -1 ? req.url.length : this.questionMarkIndex)
		this.params = _.mapKeys(req.query, (v: string, k: string) =>
			k.toLowerCase()
		)
		const host = req.hostname
		const port = req.headers["x-forwarded-port"] || req.socket.localPort
		const protocol = req.headers["x-forwarded-protocol"] || req.protocol
		this.port = port === 80 || port === "443" ? "" : `:${port}`
		this.url = `${protocol}://${host}${this.port}${req.url.substring(0, req.url.indexOf("?"))}`
	}
	getParamHashed() {
		const params = stringify(this.params)
		return WebService.hashParamsString(params)
	}
	public static hashParamsString(paramsString: string): string {
		return createHash("sha1").update(paramsString).digest("hex")
	}
	public return304(): void {
		this.res.sendStatus(304)
	}
}