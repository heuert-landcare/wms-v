import express = require("express");
import * as compression from "compression"; // compresses requests
import * as bodyParser from "body-parser";
// import * as logger from "./util/logger";
import * as lusca from "lusca";
import * as dotenv from "dotenv";
import * as path from "path";
import * as expressValidator from "express-validator";
import { Request, Response } from "express"

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: ".env.example" });

// Controllers (route handlers)
import * as apiController from "./controllers/api";
import { WmsV } from "./models/wms-v";
import { WmsTime } from "./models/wms-time";
import { VideoComposite } from "./models/video-composite"

// API keys and Passport configuration
// import * as passportConfig from "./config/passport";

// Create Express server
export const app = express();

// Express configuration
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(lusca.xframe("SAMEORIGIN"));
app.use(lusca.xssProtection(true));

app.use(
	express.static(path.join(__dirname, "public"), { maxAge: 31557600000 })
);

/**
 * API examples routes.
 */
app.get("/wms-v", (req: Request, res: Response) => {
	new WmsV(req, res).get()
});
app.get("/wms", (req: Request, res: Response) => {
	new WmsTime(req, res).get()
});
app.get("/wmst", (req: Request, res: Response) => {
	new WmsTime(req, res).proxy()
})
app.get("/wms-v-composite", (req: Request, res: Response) => {
	new VideoComposite(req, res).get()
})
// app.get("/wms-v/static", apiController.wmsV);
// app.get("/wms", apiController.wms);

export default app;
