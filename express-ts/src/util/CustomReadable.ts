import { Readable } from "stream";


class CustomReadable implements Readable {
	readable: boolean; readableHighWaterMark: number;
	readableLength: number;
	_read(size: number): void {
		throw new Error("Method not implemented.");
	}
	read(size?: number) {
		throw new Error("Method not implemented.");
	}
	setEncoding(encoding: string): this {
		throw new Error("Method not implemented.");
	}
	pause(): this {
		throw new Error("Method not implemented.");
	}
	resume(): this {
		throw new Error("Method not implemented.");
	}
	isPaused(): boolean {
		throw new Error("Method not implemented.");
	}
	unpipe<T extends NodeJS.WritableStream>(destination?: T): this {
		throw new Error("Method not implemented.");
	}
	unshift(chunk: any): void {
		throw new Error("Method not implemented.");
	}
	wrap(oldStream: NodeJS.ReadableStream): this {
		throw new Error("Method not implemented.");
	}
	push(chunk: any, encoding?: string): boolean {
		throw new Error("Method not implemented.");
	}
	_destroy(err: Error, callback: Function): void {
		throw new Error("Method not implemented.");
	}
	destroy(error?: Error): void {
		throw new Error("Method not implemented.");
	}
	addListener(event: string, listener: (...args: any[]) => void): this;
	addListener(event: "close", listener: () => void): this;
	addListener(event: "data", listener: (chunk: string | Buffer) => void): this;
	addListener(event: "end", listener: () => void): this;
	addListener(event: "readable", listener: () => void): this;
	addListener(event: "error", listener: (err: Error) => void): this;
	addListener(event: any, listener: any) {
		throw new Error("Method not implemented.");
	}
	emit(event: string | symbol, ...args: any[]): boolean;
	emit(event: "close"): boolean;
	emit(event: "data", chunk: string | Buffer): boolean;
	emit(event: "end"): boolean;
	emit(event: "readable"): boolean;
	emit(event: "error", err: Error): boolean;
	emit(event: any, err?: any, ...rest?: any[]) {
		throw new Error("Method not implemented.");
	}
	on(event: string, listener: (...args: any[]) => void): this;
	on(event: "close", listener: () => void): this;
	on(event: "data", listener: (chunk: string | Buffer) => void): this;
	on(event: "end", listener: () => void): this;
	on(event: "readable", listener: () => void): this;
	on(event: "error", listener: (err: Error) => void): this;
	on(event: any, listener: any) {
		throw new Error("Method not implemented.");
	}
	once(event: string, listener: (...args: any[]) => void): this;
	once(event: "close", listener: () => void): this;
	once(event: "data", listener: (chunk: string | Buffer) => void): this;
	once(event: "end", listener: () => void): this;
	once(event: "readable", listener: () => void): this;
	once(event: "error", listener: (err: Error) => void): this;
	once(event: any, listener: any) {
		throw new Error("Method not implemented.");
	}
	prependListener(event: string, listener: (...args: any[]) => void): this;
	prependListener(event: "close", listener: () => void): this;
	prependListener(event: "data", listener: (chunk: string | Buffer) => void): this;
	prependListener(event: "end", listener: () => void): this;
	prependListener(event: "readable", listener: () => void): this;
	prependListener(event: "error", listener: (err: Error) => void): this;
	prependListener(event: any, listener: any) {
		throw new Error("Method not implemented.");
	}
	prependOnceListener(event: string, listener: (...args: any[]) => void): this;
	prependOnceListener(event: "close", listener: () => void): this;
	prependOnceListener(event: "data", listener: (chunk: string | Buffer) => void): this;
	prependOnceListener(event: "end", listener: () => void): this;
	prependOnceListener(event: "readable", listener: () => void): this;
	prependOnceListener(event: "error", listener: (err: Error) => void): this;
	prependOnceListener(event: any, listener: any) {
		throw new Error("Method not implemented.");
	}
	removeListener(event: string, listener: (...args: any[]) => void): this;
	removeListener(event: "close", listener: () => void): this;
	removeListener(event: "data", listener: (chunk: string | Buffer) => void): this;
	removeListener(event: "end", listener: () => void): this;
	removeListener(event: "readable", listener: () => void): this;
	removeListener(event: "error", listener: (err: Error) => void): this;
	removeListener(event: any, listener: any) {
		throw new Error("Method not implemented.");
	}
	pipe<T extends NodeJS.WritableStream>(destination: T, options?: { end?: boolean; }): T {
		throw new Error("Method not implemented.");
	}
	removeAllListeners(event?: string | symbol): this {
		throw new Error("Method not implemented.");
	}
	setMaxListeners(n: number): this {
		throw new Error("Method not implemented.");
	}
	getMaxListeners(): number {
		throw new Error("Method not implemented.");
	}
	listeners(event: string | symbol): Function[] {
		throw new Error("Method not implemented.");
	}
	rawListeners(event: string | symbol): Function[] {
		throw new Error("Method not implemented.");
	}
	eventNames(): (string | symbol)[] {
		throw new Error("Method not implemented.");
	}
	listenerCount(type: string | symbol): number {
		throw new Error("Method not implemented.");
	}

}