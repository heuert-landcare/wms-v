// import * as datejs from "safe_datejs"
// import * as datejs from "datejs"
const datejs = require("datejs")

export class TimeInterval {
	protected times: string[] = []
	constructor(protected intervalString: string) { }
	get(): string[] {
		const parts = this.intervalString.split("/")
		// const intervals = Number(parts[2])
		const intervalLength = Number(parts[2][1])
		let myDateDate: Date
		let myDate: string
		// const intervalTime = this.getUnitMs(parts[2])
		let stopper = 0
		const stopValue = 100000
		while (stopper < stopValue) {
			const i = stopper
			const beginTime = new Date(parts[0])
			const endTime = new Date(parts[1])
			const hours: number = beginTime.getUTCHours()
			switch (parts[2][2]) {
				case "S": // second
					myDateDate = beginTime.add(i * intervalLength).seconds()
					break
				case "m": // minute
					myDateDate = beginTime.add(i * intervalLength).minutes()
					break
				case "h": // hour
					myDateDate = beginTime.add(i * intervalLength).hours()
					break
				default:
				case "D": // day
					myDateDate = beginTime.add(i * intervalLength).days()
					myDateDate.setUTCHours(hours)
					break
				case "W": // week
					myDateDate = beginTime.add(i * intervalLength).weeks()
					myDateDate.setUTCHours(hours)
					break
				case "F": // fortnight
					myDateDate = beginTime.add(i * intervalLength * 2).weeks()
					myDateDate.setUTCHours(hours)
					break
				case "M": // month
					myDateDate = beginTime.add(i * intervalLength).months()
					myDateDate.setUTCHours(hours)
					break
				case "Y": // year
					myDateDate = beginTime.add(i * intervalLength).years()
					myDateDate.setUTCHours(hours)
					break
				case "Q": // quarter
					myDateDate = beginTime.add(i * intervalLength * 3).months()
					myDateDate.setUTCHours(hours)
					break
			}
			if (myDateDate.isAfter(endTime)) {
				stopper = stopValue + 1 // just to make it obvious and at the same time avoid DOS with an infinite loop!
				break
			}
			myDate = myDateDate.toISOString()
			this.times.push(myDate)
			stopper++
		}
		return this.times
	}
	public static getIntervalsFromQueryString(qs: string): string[] {
		const parts = qs.split(",")
		return parts.map(ts => new TimeInterval(ts).get()).reduce((acc, c) => acc.concat(c), [])
	}
}
