"use strict";

import async = require("async")
import request = require("request")
import * as process from "child_process"
import { Response, Request, NextFunction } from "express";
import * as fs from "fs"
import { DOMParser } from "xmldom"
import * as xpath from "xpath"
import * as _ from "lodash"
import * as https from "https"
import * as util from "util"
import { exec, spawn } from "child_process"
import multistream = require("multistream")
import { app } from "../app"

function expand(time: string[]) {
	const period = time.length === 3 ? time[2] : ""
	const start = time[0]
	const end = period ? time[1] : start
	const days = period ? Number(period.substring(1, 2)) : 0
	return {
		start: new Date(start),
		end: new Date(end),
		days
	}
}