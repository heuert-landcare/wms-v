import 'ol/ol.css';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import { Image as ImageLayer, Tile as TileLayer } from 'ol/layer.js';
import Projection from 'ol/proj/Projection.js';
import ImageWMS from 'ol/source/ImageWMS.js';
import TileWMS from 'ol/source/TileWMS.js';


var layers = [
    new ImageLayer({
        source: new ImageWMS({
            attributions: '© <a href="http://www.geo.admin.ch/internet/geoportal/' +
                'en/home.html">National parks / geo.admin.ch</a>',
            crossOrigin: 'anonymous',
            params: { 'LAYERS': 'ch.bafu.schutzgebiete-paerke_nationaler_bedeutung' },
            serverType: 'mapserver',
            url: 'https://wms.geo.admin.ch/'
        })
    })
];

// A minimal projection object is configured with only the SRS code and the map
// units. No client-side coordinate transforms are possible with such a
// projection object. Requesting tiles only needs the code together with a
// tile grid of Cartesian coordinates; it does not matter how those
// coordinates relate to latitude or longitude.
var projection = new Projection({
    code: 'EPSG:21781',
    units: 'm'
});

var map = new Map({
    layers: layers,
    target: 'map',
    view: new View({
        center: [660000, 190000],
        // center: [0, 0],
        projection: projection,
        zoom: 9
    })
});