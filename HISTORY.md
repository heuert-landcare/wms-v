# Draft

## WMS-T and WMS-V (Web Mapping - Time and Video)

Just thought I'd document my journey with WMS-V so far and any future developments here.

### Log

The initial idea came from Robert Gibb quite some time back. I do not remember the day it happened, but we were probably talking about DGGS in its early stages and raster d ata and I had mentioned HTML 5 \<video\> elements. In the conversation Robert challenged whether map data could be encoded in a video and therefore take advantage of longitudinal compression. This also has the advantage that images can be streamed to a client like a web browser. Video formats usually store key frames as a raster image, in its most primitive form stored as a JPEG, and then just store the differences to these key frames. Having spatial data that is similar but not equal over time would actually fit this technology as Robert and I quickly realised.

That started my involvement with the National Possum Model, which was a successful model and paper led by James Shepherd and others. We have a good group of scientists involved in this work around pest irradiation in New Zealand and natural preservation of native species. Visualising spatial data is and will be very important with the growth in amounts and collection of spatial raster data and its related models.

However, I realised quite a while ago, that technology in that area can provide quite some impact. With the help also from David Medyckyj-Scott as a product driver, Sam Gillingham as a GIS programmer, Andrew Cowie as a cartographer and MapServer/MapCache specialist, Stephen Campbell as a *nix guru, helping with many of the technical aspects, not to mention the NeSI platform, we managed to put together the NPM web applications at https://npm.landcareresearch.co.nz/possums/anim/swipe2.php and https://npm.landcareresearch.co.nz/ . This was merely a proof-of-concept application and we took all possible shortcuts. I hacked MapCache to serve up the correct headers for HTML 5 video and understand the URL parameters and basically just wrote a bunch of BASH scripts with command line versions of mapserv and ffmpeg concatenating the raster PNG images to videos. There were so many files that we ran into inode limitations. This was not the right way to do it, but we have very limited funding and were basically doing it for passion and hobby.

David contributed heavily to motivating a standards driven approach. And after various conferences and discussions around standards with mainly Alistair Ritchie and David, and actually mainly frustrations with non-standard power plugs in various countries, I got hooked on standards and their benefits.

So, now I'm ambitiously working towards a much better presentation at FOSS4G Oceania. I have implemented a synchronisation algorithm, which seems to work well in the browsers I tried and making WMS-V dynamic! I have now a new data set from Pierre Roudier. It is essentially temperature data of Antarctica over one year. He said he has more data, but we thought we'd start with that.