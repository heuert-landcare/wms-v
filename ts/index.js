//Requires
var express = require('express');
var ejs = require('ejs');
var stream = require('express-stream');
 
//App setup
var app = express();
app.set('views', './views');
app.set('view engine', 'ejs');
 
//App-wide streaming setup
stream.useAllAutoTags(true);
stream.streamBefore('pre-body-view');
stream.streamAfter('post-body-view');
 
//Add the middleware to the desired routes
app.get('/', stream.stream(), function (req, res) {
  res.render('landing'); //This route will now stream
});
