"use strict";
exports.__esModule = true;
// import * as https from 'https'
var fs = require("fs");
var xmldom_1 = require("xmldom");
var xpath = require("xpath");
var file = '../earth_observations_wms.xml';
var encoding = 'utf8';
var select = xpath.useNamespaces({ w: 'http://www.opengis.net/wms' });
var url = 'https://neo.sci.gsfc.nasa.gov/wms/wms?version=1.3.0&service=WMS&request=GetMap&layers=MOD11C1_E_LSTDA&crs=CRS:84&bbox=0,10,60,50&width=1920&height=1080&format=image/png&time=2000-02-26T00:00:00';
function expand(time) {
    var period = time.length === 3 ? time[2] : '';
    var start = time[0];
    var end = period ? time[1] : start;
    var days = period ? Number(period.substring(1, 2)) : 0;
    console.log(days);
    return {
        start: new Date(start),
        end: new Date(end),
        days: days
    };
}
fs.readFile(file, encoding, function (err, text) {
    var parser = new xmldom_1.DOMParser();
    var doc = parser.parseFromString(text, 'application/xml');
    var dimensionNode = select('//w:Name[text()="MOD11C1_E_LSTDA"]/../w:Dimension[@name="time"]', doc, true);
    var timesString = dimensionNode.childNodes[0].nodeValue;
    // console.log(dimensionNode.childNodes[0].nodeValue)
    var times = timesString.split(',').map(function (interval) { return interval.split('/'); });
    // console.log(times)
    var intervals = times.map(function (t) { return expand(t); });
    console.table(intervals);
});
//# sourceMappingURL=server.js.map