# Presentation Notes

## Slide 0 - Introduction

(not MWLR!)
Hi, I'm Tim,
I work for Manaaki Whenua - Landcare Research, New Zealand.

Let me take you on a journey with geographical videos and longitudinal
compression and a story about my experience with this exciting, new
technology.

## Slide 1 - Summary

In summary:
I want to briefly explain the motivation for WMS-V. 
Then I'd like to show you a few proof of concept
applications, the first one of which was the visualisation
of our National Possum Model.
Furthermore, I attempted visualising temperature
change in the Antarctic in 2015. The challenges of that
data led me to a visually nicer data set: The Himawari
Satellite takes a photo every 10 minutes which I downloaded
a sample of and concatenated to videos.

I have a few words on more challenges as well.

Meanwhile, I will try to summarise the story of WMS-V and briefly
cover some concepts and considerations.

And finally propose one or more standards.

## Slide 2 - Why

First: The why!

Visualisation of temporal data over time such as weather patterns,
forecasts, bush fires, storms or different properties such as
soil profiles, travelling through depth in the ground,
water or animal bodies could really help us see what is going on
in very interesting ways across different domains.

In general, with what we perceive as time, we can
visualise at least one additional dimension of data,
whether this is in 2D or 3D, on a screen or potentially even in
Virtual Reality.

For now, let's start with 2D maps and data over time in an
interactive way.

## Slide 3 - WMS-Time

WMS-Time is a standard in the Web Mapping Service framework.
It is a standard which enables open source
maps to consider time as simple map tiles.

Here you can see an example using WMS Time and PNG images.

As you may be able to see, we are making 110 requests here for one layer and one extent!
I don't think I need to explain why this is a problem.
There is no transition between frames and no server side filtering
between images possible.

Not only latency, but also decoding compressed images is costly.
So why not have a stream of
images coming in as a video and being decoded as a stream?
Video technology has been researched and improved upon over
decades now and we have very efficient video formats, both in terms
of speed and file size.

Why visualise raster data?

A lot of data is sourced as raster.
Aerial, satellite or UAV imagery to name just a few.

## Slide 4 - Model image?

Models can be less costly to process using rasters, since
some operations are costly to calculate with big data
and as vectors.

Let me show you the National Possum Model.

I won't go into too much detail of the model.
The focus here is on the video map technology.

## Slide 5 - NPM Video

As you can see ...

There is a navigable map, showing the videos vs the map tiles.
The extent and positions of video tiles are shown accurately on screen.

Regional council areas and their statistics are shown.

There is a slider to advance time in a live preview.

## Slide 6 - How?

How did we do this?

Initially, with the Possum Model, we replaced all the image elements
with HTML5 video elements. In OpenLayers 2 at the time, DOM-rendering was used, which was the only approach possible at the time.

DOM-rendering is expensive and was later replaced by Canvas or WebGL rendering.
An HTML Canvas element enables developers to do filtering and advanced
image manipulations, accelerated by the GPU and not having to worry 
about all graphics challenges.

My namesake Tim Schaub put together a proof of concept, which I learnt was
inspired by a MapBox post before, but after we did the National Possum
Model visualisation.

## Slide 7 - Tim Schaub Interactive Video Demo

Here is the example he put together.

You can see how this kind of visual could not be achieved with downloading
PNG images as frames or with DOM rendering with this kind of performance
and at an angle.

I'll let you appreciate this for a few seconds...

## Slide 8 - Other Data

I tried other data.
The remotely sensed Antarctic Surface Temperature in 2015.
However, the data was not really suitable.
So, WMS-V is not great for visualising data that is too far apart
in terms of time to show smooth transitions.
Also, some caching should be done. Video tiles and compositing 
them on the server may be
the way to go. Cropping, composing and padding works.
Ffmpeg and complex filters make this possible.

## Slide 9 - Challenges

Transparent videos are not fully supported at the time I checked.

With the Antarctic data I wrote a client side
filter that uses the green channel as transparency which of course
only works if you are not using a channel in your data visualisation.

Layers on top of the video layer are problematic without DOM rendering.

Synchronising video tiles is a really great challenge. The browser
APIs are not adequate for this purpouse.
I tried a lot of things. Having the videos
pre-computed and sitting on the server as simple files was best for now.
Downloading partial content then gets handled by the web server.

I spent a lot of time optimising the synchronisation. In the end,
double buffering and frame caching seems to work best and surprisingly
also seems OK in terms of memory use.

Performance and stability are challenging as well. Having tabs open
for a while can crash the browser. At FOSS4G in 2013,
my demo failed, because I had opened it in the wrong browser.

I've been implementing a dynamic WMS-V that works
with any bounding box, so that videos can be encoded on the fly. This
solves the synchronisation problem, but increases the load
on the server and server side filtering such as blending can slow things
down not to forget image loading.
FFMPEG supports GPU rendering, but it makes the video quality worse.
There may be faster video encoding options, libraries or similar that
can solve these problems.

Encoding one video and streaming it was not the bottle neck! It is
the image tile delivery coming in when being behind a WMS-Time.
So, if one could use the FFMPEG or other API
on a low level and access the raster data with indexes directly, I
believe on-the-fly delivery could be the way to go.

## Slide 10 - Story 1 - History

So, now more to the story...

In 2012, Landcare was involved with the Possum Model...

I remember talking to a colleague about this exciting new HTML 5
technology, video and canvas describing how these elements
behave like images in many ways. In the conversation the idea of
using video tiles came up.

Robert coined the phrase "longitudinal compression".

Many hours of "procrastiworking" later, I had created
some map video tiles from the possum model raster data and combined
some on a small grid in my browser, playing the videos at the same time.

The model got some interest within the company and wider circles.
However, the bulk of the time was in late nights at home and
whenever I could fit it in.

## Slide 11 - Story 2 - FOSS4G 2013, Motivation

When I presented WMS-V in 2013, I was talking to the OpenLayers guys
and Tim Schaub came up with a post-compose idea
and building it as an extension to OpenLayers.

Later yet again, there was more motivation to pursue this side
project and present again here today.

## Slide 12 - Standards

Now to Standards!

I have a long and skeptical journey with standards. It began when
David Medyckyj-Scott gave the immense job of reading the
WMS spec and later punished me with the even more convoluted WFS
standard. At the time, as you can imagine, I did not appreciate
standards much.

However, having spent time with WFS 3 at a hackathon in
Fort Collins and getting annoyed yet again with
the different power plugs in different countries, ...
... I realised more and more that standards are important.

## Slide 13 - WMS-Time

So, I spent a bit of WMS-Time, pun intended. I hacked it behind my
WMS-V and learnt to appreciate the ISO time notation standards
since they are even implemented in JS.

I had a look at the time range spec in WMS-Time and decided to implement
that in my dynamic WMS-V... And here it is!

You can find the details and how to interpret them online.

## Slide 14 - More to standardise?

More that could be standardised:

Frame rate, video formats, encoding options, filter spec. All
this as extensions or core.

## Slide 15 - Stream video

If you're not asleep yet, this might help...

## Slide 16 - Story: Challenges

Finding good example data is more difficult than I thought.
I was not very happy with what I had to show. So, I found
the Himawari Satellite imagery, which you can see in a minute.

You could imagine that you could make a zoomable, panable nk video
possible with WMS-V, just as a side note.

## Slide 17 - Satellite video

Satellite video feeds could provide live
coverage of world events such as forrest fires or other
natural disasters.

Weather data is shown on maps in every day TV in form of
weather forecasts.

All this and more could be visualised with remotely sensed
or modelled raster data and WMS-V.

## Slide 18 - Himawari client tiles

Here the promised Himawari footage as a web app and using video
tiles. Getting them synchronising like this was a major challenge.

## Slide 19 - Himawari 1080 slice

And here a 1080p slice of it. As you can see, the weather is nicely
visible here.

## Slide 20 - Cropped video on-the-fly

Just a couple of days ago, I got a dynamic version of the Himawari
videos going where I simply crop a high-resolution video on the fly.
It basically projects a portion of the video for display.

## Slide 21 - Conclusion

So, consider WMS-V as something on your radar.

Thank you! And here I open for questions.